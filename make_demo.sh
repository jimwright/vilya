#!/usr/bin/env bash

vilya system define incl00pa00 --copy default --incl  0 --posang  0
vilya system define incl30pa30 --copy default --incl 30 --posang 30
vilya system define incl45pa60 --copy default --incl 45 --posang 60

vilya simulate demodisc --disc default --hydro medium --dust dsharp_i35
vilya radiate demodisc/sys1 --system default
vilya observe demodisc/sys1/alma10.5 --system default --config alma10.5
vilya observe demodisc/sys1/alma10.6 --system default --config alma10.6
vilya observe demodisc/sys1/alma10.7 --system default --config alma10.7
vilya observe demodisc/sys1/alma10.8 --system default --config alma10.8
vilya observe demodisc/sys1/dsharp1 --system default --config dsharp1
vilya observe demodisc/sys1/dsharp24 --system default --config dsharp24
vilya observe demodisc/sys1/dsharp96 --system default --config dsharp96
vilya observe demodisc/sys1/dsharp384 --system default --config dsharp384
vilya observe demodisc/sys1/incl00pa00 --system incl00pa00 --config alma10.6
vilya observe demodisc/sys1/incl30pa30 --system incl30pa30 --config alma10.6
vilya observe demodisc/sys1/incl45pa60 --system incl45pa60 --config alma10.6
