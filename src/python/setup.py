from setuptools import setup

setup(
    name='vilya',
    version='0.0.1',
    author='James Wright',
    author_email='vilya@jim-wright.com',
    packages=['vilya'],
    entry_points = {
        'console_scripts': [
            'vilya = vilya.cli:execute',
            'fargo3d-wrapper = vilya.fargo:restartable_wrapper',
        ],
    },
    install_requires=[
        'numpy','matplotlib'
    ]
)
