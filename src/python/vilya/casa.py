# -*- coding: utf-8 -*-

import argparse
import os

import numpy as np

import astropy.units as u
from astropy.units import Quantity, Unit
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy.io import fits
from astropy.nddata import Cutout2D
from astropy.visualization import AsinhStretch, ImageNormalize, ManualInterval

from radmc3dPy import image

from .system import SystemDatabase

class CasaConfig:
    @classmethod
    def generate_makefile(cls, system, config, obspath, overwrite):
        obspath = os.path.abspath(obspath)
        obsname = os.path.basename(obspath)
        radpath = os.path.normpath(os.path.join(obspath, '..'))
        radname = os.path.basename(radpath)

        with open(os.path.join(radpath, f'Makefile-{obsname}'), 'w') as makefile:
            makefile.write(f'''\
ALL_TARGETS += {radname}/casa-{obsname}.png

{radname}/casa-{obsname}.png: {radname}/dust_temperature.bdat
\t@echo "### Running simulated ALMA observation using CASA"
\tcd {radname} && python -m vilya.casa --system {system.name} --config {config} $(SIM_PATH)/{radname}/{obsname} >>casa-{obsname}.log 2>&1
''')

def export_image(args, system, fits_fn, format='png'):
    import matplotlib.pyplot as plt

    dist = system.distance   # Quantity("121pc")
    width = 2 * 1.5 * system.planet_sma  # Quantity("99AU") * 2 * 1.5
    ang_width = (width/dist).to('') * u.rad
    size = u.Quantity((ang_width, ang_width), u.arcsec)

    ff = fits.open(fits_fn)
    data = ff[0].data.squeeze() # drops the size-1 axes
    wcs = WCS(ff[0].header).celestial

    position = wcs.pixel_to_world(data.shape[0]/2, data.shape[1]/2)
    cutout = Cutout2D(data, position, size, wcs=wcs)

    cdata = 1000 * cutout.data      # Convert from Jy to mJy
    vmin = 0.0
    vmax = np.max(cdata)
    norm = ImageNormalize(cdata, interval=ManualInterval(vmin=vmin, vmax=vmax), stretch=AsinhStretch())

    x = (np.arange(cdata.shape[0]+1)/cdata.shape[0] - 0.5) * width
    y = (np.arange(cdata.shape[1]+1)/cdata.shape[1] - 0.5) * width
    X, Y = np.meshgrid(x, y)

    plt.figure(figsize=(7,6))
    plt.tight_layout()
    plt.gca().set_aspect("equal")
    plt.pcolormesh(X, Y, cdata, norm=norm, cmap=plt.cm.magma, rasterized=True)
    plt.xlabel('AU')
    plt.ylabel('AU')
    plt.colorbar(label='$S_{\\nu}$ (mJy / beam)')
    plt.tight_layout()
    plt.savefig(os.path.splitext(fits_fn)[0] + '.' + format, format=format, bbox_inches='tight')

def generate_skymodel(args, system, skymodel_fits):
    if os.path.exists('image.out'):
        os.remove('image.out')

    try:
        dpc = system.distance.to('pc')
        coord = system.coord
        posang = system.posang + 90
        incl = system.incl

        image.makeImage(npix=args.num_pixels, incl=incl, posang=posang, wav=1.2e3, nostar=True)   # This calls radmc3d 
        im_mm = image.readImage()
        im_mm.writeFits(skymodel_fits, dpc=dpc.value, coord=coord)
    finally:
        if os.path.exists('image.out'):
            os.remove('image.out')

def generate_casa(args, system, skymodel_fits, casa_fits, obsname):
    # We delay importing casatasks until we are in the correct directory
    from casatasks import simalma, exportfits
    import matplotlib.pyplot as plt
    plt.rcParams["figure.figsize"] = (10,6)

    ff = fits.open(skymodel_fits)
    data = ff[0].data.squeeze() # drops the size-1 axes
    wcs = WCS(ff[0].header).celestial

    imsize = data.shape
    p1 = wcs.pixel_to_world(0, 0)
    p2 = wcs.pixel_to_world(*imsize)
    mapsize_ra = Quantity(p1.ra - p2.ra).to(u.arcsec)
    mapsize_dec = Quantity(p2.dec - p1.dec).to(u.arcsec)
    mapsize = mapsize_dec
    incell = mapsize/data.shape[0]

    # Default cleaning parameters
    pwv = 0.5
    niter = 50

    # Choose different observing configurations
    if args.config[:4] == 'alma':
        if len(args.config) == 4:
            cfg = '10.6'
        else:
            cfg = args.config[4:]
        antennalist = f'alma.cycle{cfg}.cfg'
        totaltime = '2700s'
        integration = '300s'
        inwidth = '50.00MHz'
        outname = f'alma.cycle{cfg}.noisy'
    elif args.config[:6] == 'dsharp':
        # Andrews 2018, however simalma doesn't accept nchan, so "fake" it
        # by extending the total time.  Also in reality each channel is
        # unlikely to have totally independent noise, so only consider one
        # "SPW" of 128 faked channels.
        if len(args.config) == 6:
            nchan = 384
        else:
            nchan = int(args.config[6:])
        antennalist = ['alma.cycle10.5.cfg', 'alma.cycle10.8.cfg']
        totaltime = [f'{nchan * 12 * 60}s', f'{nchan * 2 * 36 * 60}s']
        integration = '360s'    # A common division of the totaltimes
        inwidth = '31.25MHz'
        outname = 'concat'
    else:
        raise ValueError(f'Unknown observing config {args.config}')

    print("In Width     =", inwidth)
    print("Antenna List =", antennalist)
    print("Total Time   =", totaltime)
    print("Integration  =", integration)
    print("PWV          =", pwv)
    print("NIter        =", niter)

    # Run simalma
    simalma(
        project = obsname,
        skymodel = skymodel_fits,
        mapsize = f'{mapsize.value:g}arcsec',
        incell = f'{incell.value:g}arcsec',
        inwidth = inwidth,
        imsize = imsize,

        integration = integration,
        antennalist = antennalist,
        totaltime = totaltime,

        pwv = pwv,
        niter = niter,

        dryrun = False,
        overwrite = True,
        graphics = 'file',
        verbose = True,
    )

    exportfits(
        imagename = os.path.join(obsname, f'{obsname}.{outname}.image.flat'),
        fitsimage = casa_fits,
        overwrite = True,
    )


def execute():
    parser = argparse.ArgumentParser()
    parser.add_argument('--system', type=str, metavar='SYSTEM_NAME')
    parser.add_argument('--num-pixels', type=int, default=2500, metavar='PIXELS')
    parser.add_argument('--config', type=str, default='alma', metavar='CONFIG')
    parser.add_argument('obspath', type=str, metavar='OBSPATH')
    args = parser.parse_args()

    obspath = os.path.abspath(args.obspath)
    obsname = os.path.basename(obspath)
    radpath = os.path.abspath(os.path.join(args.obspath, '..'))
    os.chdir(radpath)

    system_db = SystemDatabase()
    system = system_db.load(args.system)

    skymodel_fits = f'skymodel-{args.system}.fits'
    skymodel_png = f'skymodel-{args.system}.png'
    skymodel_pdf = f'skymodel-{args.system}.pdf'
    casa_fits = f'casa-{obsname}.fits'
    casa_png = f'casa-{obsname}.png'
    casa_pdf = f'casa-{obsname}.pdf'

    # Ensure sky model exists
    if not os.path.exists(skymodel_fits):
        generate_skymodel(args, system, skymodel_fits)
    if not os.path.exists(skymodel_png):
        export_image(args, system, skymodel_fits, format='png')
    if not os.path.exists(skymodel_pdf):
        export_image(args, system, skymodel_fits, format='pdf')

    # Ensure casa model exists
    if not os.path.exists(casa_fits):
        generate_casa(args, system, skymodel_fits, casa_fits, obsname)
    if not os.path.exists(casa_png):
        export_image(args, system, casa_fits, format='png')
    if not os.path.exists(casa_pdf):
        export_image(args, system, casa_fits, format='pdf')

if __name__ == '__main__':
    execute()
