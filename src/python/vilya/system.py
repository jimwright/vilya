# -*- coding: utf-8 -*-

from enum import Enum, unique
import glob
import json
import os
import re
import textwrap

import numpy as np

import astropy.constants as apc
import astropy.units as apu
from astropy.units import Quantity, Unit

from .utils import parse_value
from . import VILYA_DEFNS_PATH

class SystemDatabase:
    # Construction
    # =========================================================================
    def __init__(self, path=os.path.join(VILYA_DEFNS_PATH, 'system')):
        self._path = path

    def save(self, system, overwrite=False):
        filename = self._make_filename(system.name)
        if os.path.exists(filename) and not overwrite:
            raise FileExistsError(filename)
        json_string = system.to_json()
        with open(filename, 'w') as file:
            file.write(json_string + '\n')

    def load(self, name):
        filename = self._make_filename(name)
        with open(filename) as file:
            json_string = ''.join(file.readlines())
        return System.from_json(json_string)

    def delete(self, name):
        filename = self._make_filename(name)
        os.remove(filename)

    def list(self):
        result = []
        for filename in glob.glob(self._make_filename('*', validate=False)):
            system_name = os.path.splitext(os.path.basename(filename))[0]
            result.append(system_name)
        return result

    # Validation
    # =========================================================================
    @classmethod
    def validate_name(cls, name):
        # Needs to be something that is valid in the filing system
        if not re.match('^[0-9A-Za-z_:=,@+-]+$', name):
            raise ValueError(f'System name {name} contains an invalid character')
        
    # Helper functions
    # =========================================================================
    def _make_filename(self, system_name, validate=True):
        if validate:
            self.validate_name(system_name)
        return os.path.join(self._path, system_name + '.json')

class System:
    DEFAULT_INCL = 0.0
    DEFAULT_POSANG = 0.0
    DEFAULT_COORD = '23h59m59.96s -34d59m59.50s'

    # Construction
    # =========================================================================
    def __init__(self,
                 name = None,
                 desc = None,
                 distance = None,
                 coord = None,
                 incl = None,
                 posang = None,
                 mstar = None,
                 rstar = None,
                 tstar = None,
                 planet_sma = None,
                 defaults = None):

        # Descriptive properties
        name, desc = self._parse_descriptive(
            name = name,
            desc = desc,
            defaults = defaults)

        self._name = name
        self._desc = desc

        # System properties
        distance, coord, incl, posang = self._parse_system(
            distance = distance,
            coord = coord,
            incl = incl,
            posang = posang,
            defaults = defaults)

        if distance.unit.is_unity(): distance *= Unit('pc')

        self._distance = distance.to('pc')
        self._coord = coord
        self._incl = incl
        self._posang = posang

        # Star properties
        mstar, rstar, tstar = self._parse_star(
            mstar = mstar,
            rstar = rstar,
            tstar = tstar,
            defaults = defaults)

        if mstar.unit.is_unity(): mstar *= Unit('Msun')
        if rstar.unit.is_unity(): rstar *= Unit('Rsun')
        if tstar.unit.is_unity(): tstar *= Unit('K')

        self._mstar = mstar.to('Msun')
        self._rstar = rstar.to('Rsun')
        self._tstar = tstar.to('K')

        # Planet properties
        planet_sma = self._parse_planet(
            planet_sma = planet_sma,
            defaults = defaults)

        if planet_sma.unit.is_unity(): planet_sma *= Unit('AU')

        self._planet_sma = planet_sma.to('AU')

    @classmethod
    def _parse_descriptive(cls, name, desc, defaults=None):
        name = parse_value(name, None, defaults, 'name', ctor=str)
        desc = parse_value(desc, None, defaults, 'desc', ctor=str)

        SystemDatabase.validate_name(name)

        return name, desc

    @classmethod
    def _parse_system(cls, distance, coord, incl, posang, defaults=None):
        distance = parse_value(distance, None, defaults, 'distance', ctor=Quantity)
        coord = parse_value(coord, cls.DEFAULT_COORD, defaults, 'coord', ctor=str)
        incl = parse_value(incl, cls.DEFAULT_INCL, defaults, 'incl', ctor=float)
        posang = parse_value(posang, cls.DEFAULT_POSANG, defaults, 'posang', ctor=float)

        return distance, coord, incl, posang

    @classmethod
    def _parse_star(cls, mstar, rstar, tstar, defaults=None):
        # Geometric properties
        mstar = parse_value(mstar, None, defaults, 'mstar', ctor=Quantity)
        rstar = parse_value(rstar, None, defaults, 'rstar', ctor=Quantity)
        tstar = parse_value(tstar, None, defaults, 'tstar', ctor=Quantity)

        return mstar, rstar, tstar

    @classmethod
    def _parse_planet(cls, planet_sma, defaults=None):
        # Composition properties
        planet_sma = parse_value(planet_sma, None, defaults, 'planet_sma', ctor=Quantity)

        return planet_sma

    # Descriptive properties
    # =========================================================================
    @property
    def name(self):
        return self._name

    @property
    def desc(self):
        return self._desc

    # Unit properties
    # =========================================================================
    @property
    def distance(self):
        return self._distance

    @property
    def coord(self):
        return self._coord

    @property
    def incl(self):
        return self._incl

    @property
    def posang(self):
        return self._posang

    # Star properties
    # =========================================================================
    @property
    def mstar(self):
        return self._mstar

    @property
    def rstar(self):
        return self._rstar

    @property
    def tstar(self):
        return self._tstar

    # Planet properties
    # =========================================================================
    @property
    def planet_sma(self):
        return self._planet_sma

    # Formatting methods
    # =========================================================================
    def __str__(self):
        return textwrap.dedent(f'''\
            NAME:   {self.name}
            DESC:   {self.desc}
            SYSTEM: dist={self.distance:g}, coord={self.coord}
            ORIENT: incl={self.incl:g}, posang={self.posang:g}
            STAR:   mstar={self.mstar:g}, rstar={self.rstar:g}, tstar={self.tstar:g}
            PLANET: sma={self.planet_sma:g}''')

    def __repr__(self):
        return (f'{self.__class__.__name__}('
            f'name={self.name!r}, '
            f'desc={self.desc!r}, '
            f'distance={self.distance!r}, '
            f'coord={self.coord!r}, '
            f'incl={self.incl!r}, '
            f'posang={self.posang!r}, '
            f'mstar={self.mstar!r}, '
            f'rstar={self.rstar!r}, '
            f'tstar={self.tstar!r}, '
            f'planet_sma={self.planet_sma!r}')

    # Serialisation methods
    # =========================================================================
    def to_json(self):
        fmt_qty = lambda q: f'{q.value:g} {q.unit.to_string()}'.strip()

        return json.dumps({
            'version': 1,
            'name': self.name,
            'desc': self.desc,
            'distance': fmt_qty(self.distance),
            'coord': self.coord,
            'incl': self.incl,
            'posang': self.posang,
            'mstar': fmt_qty(self.mstar),
            'rstar': fmt_qty(self.rstar),
            'tstar': fmt_qty(self.tstar),
            'planet_sma': fmt_qty(self.planet_sma),
        })

    @classmethod
    def from_json(cls, value):
        kwrds = json.loads(value)
        version = kwrds.pop('version')
        return cls(**kwrds)
