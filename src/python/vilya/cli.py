# -*- coding: utf-8 -*-

import argparse
import glob
import fnmatch
import os
from tabulate import tabulate

from .dust import Dust, DustDatabase
from .disc import Disc, DiscDatabase, DiscUnits
from .hydro import HydroPreset, Hydro, HydroDatabase
from .system import System, SystemDatabase

from . import VILYA_RESULTS_PATH
from .fargo import FargoConfig
from .radmc import RadmcConfig
from .casa import CasaConfig

dust_db = DustDatabase()
disc_db = DiscDatabase()
hydro_db = HydroDatabase()
system_db = SystemDatabase()

# Construct parser
# =============================================================================
def setup_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
        title='commands',
        description='valid subcommands',
        dest='subparser1')

    setup_dust_parser(subparsers.add_parser(
        'dust',
        help='manipulate dust definitions'))

    setup_disc_parser(subparsers.add_parser(
        'disc',
        help='manipulate disc definitions'))

    setup_hydro_parser(subparsers.add_parser(
        'hydro',
        help='manipulate hydro simulation setups'))

    setup_system_parser(subparsers.add_parser(
        'system',
        help='manipulate system definitions'))

    setup_simulate_parser(subparsers.add_parser(
        'simulate',
        help='configure a simulation'))

    setup_radiate_parser(subparsers.add_parser(
        'radiate',
        help='configure radiative tranfer'))

    setup_observe_parser(subparsers.add_parser(
        'observe',
        help='configure an observation'))

    return parser

# Construct dust command parser
# =============================================================================
def setup_dust_parser(parser):
    subparsers = parser.add_subparsers(
        title='dust command',
        description='valid dust commands',
        dest='subparser2')

    setup_dust_define_parser(subparsers.add_parser(
        'define',
        help='define a new dust definition'))

    setup_dust_list_parser(subparsers.add_parser(
        'list',
        help='list dust definitions'))

    setup_dust_show_parser(subparsers.add_parser(
        'show',
        help='show dust definitions'))

    setup_dust_remove_parser(subparsers.add_parser(
        'remove',
        help='remove dust definitions'))

def setup_dust_define_parser(parser):
    group = parser.add_argument_group('descriptive')
    group.add_argument('name',
                       type=str,
                       help='name of dust being defined')
    group.add_argument('--desc',
                       metavar='DESCRIPTION',
                       default='',
                       type=str,
                       help='freeform description of the dust')
    group.add_argument('--copy',
                       metavar='NAME',
                       type=str,
                       help='use a copy of NAME as the defaults/starting point')

    group = parser.add_argument_group('size')
    default = f'{Dust.DEFAULT_MIN_SIZE}'
    group.add_argument('--min-size',
                       metavar='MIN-SIZE',
                       help=f'minimum grain size [DEFAULT {default}]')
    default = f'{Dust.DEFAULT_MAX_SIZE}'
    group.add_argument('--max-size',
                       metavar='MAX-SIZE',
                       help=f'maximum grain size [DEFAULT {default}]')
    default = f'{Dust.DEFAULT_POWER_INDEX}'
    group.add_argument('--power-index',
                       metavar='INDEX',
                       help=f'mass density power index [DEFAULT {default}]')

    group = parser.add_argument_group('material')
    default = f'{Dust.DEFAULT_MATERIAL}'
    group.add_argument('--material',
                       metavar='MAT=MFRAC,...',
                       help=f'grain core material [DEFAULT {default}]')
    default = f'{Dust.DEFAULT_POROSITY}'
    group.add_argument('--porosity',
                       metavar='VFRAC',
                       help=f'grain core porosity [DEFAULT {default}]')
    default = f'material/porosity dependent'
    group.add_argument('--grain-rho',
                       metavar='DENSITY',
                       help=f'grain core rho [DEFAULT {default}]')

def setup_dust_list_parser(parser):
    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='*',
                       type=str,
                       help='names of dusts to be listed')

def setup_dust_show_parser(parser):
    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='+',
                       type=str,
                       help='names of dusts to be displayed')

def setup_dust_remove_parser(parser):
    parser.add_argument('--dry-run', '-n',
                        default=False,
                        action='store_true',
                        help='Do not remove dust, just display')

    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='+',
                       type=str,
                       help='names of dusts to be removed')

# Construct disc command parser
# =============================================================================
def setup_disc_parser(parser):
    subparsers = parser.add_subparsers(
        title='disc command',
        description='valid disc commands',
        dest='subparser2')

    setup_disc_define_parser(subparsers.add_parser(
        'define',
        help='define a new disc definition'))

    setup_disc_list_parser(subparsers.add_parser(
        'list',
        help='list disc definitions'))

    setup_disc_show_parser(subparsers.add_parser(
        'show',
        help='show disc definitions'))

    setup_disc_remove_parser(subparsers.add_parser(
        'remove',
        help='remove disc definitions'))

def setup_disc_define_parser(parser):
    group = parser.add_argument_group('descriptive')
    group.add_argument('name',
                       type=str,
                       help='name of disc being defined')
    group.add_argument('--desc',
                       metavar='DESCRIPTION',
                       default='',
                       type=str,
                       help='freeform description of the disc')
    group.add_argument('--copy',
                       metavar='NAME',
                       type=str,
                       help='use a copy of NAME as the defaults/starting point')

    group = parser.add_argument_group('units')
    default = f'{Disc.DEFAULT_UNITS}'
    group.add_argument('--units',
                       choices=['scale_free', 'cgs', 'mks'],
                       help=f'units system used to describe disc [DEFAULT {default}]')
    default = f'{Disc.DEFAULT_SCALEFREE_MSTAR:g}'
    group.add_argument('--mstar',
                       metavar='MASS',
                       help=f'central stellar mass [DEFAULT {default}]')

    group = parser.add_argument_group('geometry')
    default = f'{Disc.DEFAULT_ASPECT_RATIO:g},{Disc.DEFAULT_ASPECT_FLARING:g}'
    group.add_argument('--aspect',
                       metavar='RATIO[,FLARE]',
                       help=f'disc aspect ratio and flaring [DEFAULT {default}]')
    default = f'{Disc.DEFAULT_INNER_EDGE:g},{1/Disc.DEFAULT_INNER_EDGE:g}'
    group.add_argument('--edges',
                       metavar='INNER[,OUTER]',
                       help=f'inner and outer disc edge radii [DEFAULT {default}]')

    group = parser.add_argument_group('composition')
    default = f'{Disc.DEFAULT_SIGMA0:g},{Disc.DEFAULT_SIGMA_SLOPE:g}'
    group.add_argument('--sigma',
                       metavar='SIGMA0[,SLOPE]',
                       help=f'surface density profile [DEFAULT {default}]')
    default = f'{Disc.DEFAULT_DUST_RATIO:g}'
    group.add_argument('--dust-ratio',
                       metavar='RATIO',
                       help=f'ratio of surface dust density to gas [DEFAULT {default}]')

    group = parser.add_argument_group('kinematics')
    default = f'{Disc.DEFAULT_ALPHA:g}'
    group.add_argument('--alpha',
                       metavar='VISCOSITY',
                       help=f'alpha viscosity [DEFAULT {default}]')

    group = parser.add_argument_group('planets')
    default = f'{Disc.DEFAULT_PLANET_MASS:g}'
    group.add_argument('--planet-mass',
                       metavar='MASS',
                       help=f'set planet mass in system [DEFAULT {default}]')
    default = f'{Disc.DEFAULT_PLANET_SMA:g}'
    group.add_argument('--planet-sma',
                       metavar='DISTANCE',
                       help=f'set planet orbital radius in system [DEFAULT {default}]')

    group = parser.add_argument_group('permissible ranges')
    default = f'{Disc.DEFAULT_RANGE_MSTAR_MIN:g},{Disc.DEFAULT_RANGE_MSTAR_MAX:g}'
    group.add_argument('--range-mstar',
                       metavar='MIN[,MAX]',
                       help=f'min and max expected stellar mass [DEFAULT {default}]')
    default = f'{Disc.DEFAULT_RANGE_SMA_MIN:g},{Disc.DEFAULT_RANGE_SMA_MAX:g}'
    group.add_argument('--range-sma',
                       metavar='MIN[,MAX]',
                       help=f'min and max expected planet sma [DEFAULT {default}]')

def setup_disc_list_parser(parser):
    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='*',
                       type=str,
                       help='names of discs to be listed')

def setup_disc_show_parser(parser):
    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='+',
                       type=str,
                       help='names of discs to be displayed')

def setup_disc_remove_parser(parser):
    parser.add_argument('--dry-run', '-n',
                        default=False,
                        action='store_true',
                        help='Do not remove disc, just display')

    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='+',
                       type=str,
                       help='names of discs to be removed')

# Construct hydro command parser
# =============================================================================
def setup_hydro_parser(parser):
    subparsers = parser.add_subparsers(
        title='hydro simulation command',
        description='valid hydro simulation commands',
        dest='subparser2')

    setup_hydro_define_parser(subparsers.add_parser(
        'define',
        help='define a new hydro definition'))

    setup_hydro_list_parser(subparsers.add_parser(
        'list',
        help='list hydro definitions'))

    setup_hydro_show_parser(subparsers.add_parser(
        'show',
        help='show hydro definitions'))

    setup_hydro_remove_parser(subparsers.add_parser(
        'remove',
        help='remove hydro definitions'))

def setup_hydro_define_parser(parser):
    group = parser.add_argument_group('descriptive')
    group.add_argument('name',
                       type=str,
                       help='name of hydro being defined')
    group.add_argument('--desc',
                       metavar='DESCRIPTION',
                       default='',
                       type=str,
                       help='freeform description of the hydro')
    group.add_argument('--copy',
                       metavar='NAME',
                       type=str,
                       help='use a copy of NAME as the defaults/starting point')

    group = parser.add_argument_group('presets')
    default = f'{Hydro.DEFAULT_PRESET.name}'
    group.add_argument('--preset',
                       choices=['tiny','small','medium','large','huge'],
                       default='small',
                       help=f'quality preset [DEFAULT {default}]')

    group = parser.add_argument_group('mesh')
    default = f'preset dependent'
    group.add_argument('--mesh-cpsh',
                       metavar='CPSH',
                       help=f'cells per scale height to use at the orbital radius [DEFAULT {default}]')
    default = f'preset dependent'
    group.add_argument('--mesh-explicit',
                       metavar='NUM_AZIMUTHAL,NUM_RADIAL',
                       help=f'explicit mesh size [DEFAULT {default}]')

    group = parser.add_argument_group('limits')
    default = f'preset dependent'
    group.add_argument('--max-fluids',
                       metavar='NUM_FLUIDS',
                       type=int,
                       help=f'maximum number of fluids (gas+dust) to use in simulation [DEFAULT {default}]')
    default = f'preset dependent'
    group.add_argument('--min-edge',
                       metavar='EDGE',
                       type=float,
                       help=f'minimum edge [DEFAULT {default}]')

    group = parser.add_argument_group('orbits')
    default = f'preset dependent'
    group.add_argument('--orbits',
                       metavar='ORBITS',
                       type=int,
                       help=f'total number of orbits to simulate [DEFAULT {default}]')
    default = f'preset dependent'
    group.add_argument('--checkpoint',
                       metavar='ORBITS',
                       type=int,
                       help=f'checkpoint simulation state every ORIBITS [DEFAULT {default}]')

def setup_hydro_list_parser(parser):
    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='*',
                       type=str,
                       help='names of hydros to be listed')

def setup_hydro_show_parser(parser):
    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='+',
                       type=str,
                       help='names of hydros to be displayed')

def setup_hydro_remove_parser(parser):
    parser.add_argument('--dry-run', '-n',
                        default=False,
                        action='store_true',
                        help='Do not remove hydro, just display')

    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='+',
                       type=str,
                       help='names of hydros to be removed')

# Construct system command parser
# =============================================================================
def setup_system_parser(parser):
    subparsers = parser.add_subparsers(
        title='system command',
        description='valid system commands',
        dest='subparser2')

    setup_system_define_parser(subparsers.add_parser(
        'define',
        help='define a new system definition'))

    setup_system_list_parser(subparsers.add_parser(
        'list',
        help='list system definitions'))

    setup_system_show_parser(subparsers.add_parser(
        'show',
        help='show system definitions'))

    setup_system_remove_parser(subparsers.add_parser(
        'remove',
        help='remove system definitions'))

def setup_system_define_parser(parser):
    group = parser.add_argument_group('descriptive')
    group.add_argument('name',
                       type=str,
                       help='name of system being defined')
    group.add_argument('--desc',
                       metavar='DESCRIPTION',
                       default='',
                       type=str,
                       help='freeform description of the system')
    group.add_argument('--copy',
                       metavar='NAME',
                       type=str,
                       help='use a copy of NAME as the defaults/starting point')

    group = parser.add_argument_group('system')
    group.add_argument('--distance',
                       metavar='DISTANCE',
                       help=f'distance to system (default units pc)')
    group.add_argument('--coord',
                       metavar='RA DEC',
                       help=f'coordinate of system on sky')
    group.add_argument('--incl',
                       metavar='DEGREES',
                       help=f'inclination of system (0=face-on, 90=edge-on)')
    group.add_argument('--posang',
                       metavar='DEGREES',
                       help=f'position angle of system')

    group = parser.add_argument_group('star')
    group.add_argument('--mstar',
                       metavar='MASS',
                       help=f'mass of central star (default units Msun)')
    group.add_argument('--rstar',
                       metavar='RADIUS',
                       help=f'radius of central star (default units Rsun)')
    group.add_argument('--tstar',
                       metavar='TEMP',
                       help=f'temperature of central star (default units K)')

    group = parser.add_argument_group('planet')
    group.add_argument('--planet-sma',
                       metavar='DISTANCE',
                       help=f'set planet orbital radius in system (default units AU)')

def setup_system_list_parser(parser):
    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='*',
                       type=str,
                       help='names of systems to be listed')

def setup_system_show_parser(parser):
    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='+',
                       type=str,
                       help='names of systems to be displayed')

def setup_system_remove_parser(parser):
    parser.add_argument('--dry-run', '-n',
                        default=False,
                        action='store_true',
                        help='Do not remove system, just display')

    group = parser.add_argument_group('filters')
    group.add_argument('name',
                       nargs='+',
                       type=str,
                       help='names of systems to be removed')

# Construct simulate command parser
# =============================================================================
def setup_simulate_parser(parser):
    parser.add_argument('simpath',
                        type=str,
                        help='the name to call the simulation')

    parser.add_argument('--force',
                        default=False,
                        action='store_true',
                        help='overwrite any existing files')

    parser.add_argument('--disc',
                        type=str,
                        help='the disc configuration to use')
    parser.add_argument('--hydro',
                        type=str,
                        help='the hydro configuration to use')
    parser.add_argument('--dust',
                        type=str,
                        help='the dust configuration to use')

# Construct radiate command parser
# =============================================================================
def setup_radiate_parser(parser):
    parser.add_argument('radpath',
                        type=str,
                        help='the name to call the radiative transfer')

    parser.add_argument('--force',
                        default=False,
                        action='store_true',
                        help='overwrite any existing files')

    parser.add_argument('--system',
                        type=str,
                        help='the system configuration to use')
    parser.add_argument('--dust',
                        type=str,
                        help='the dust configuration to use')
    parser.add_argument('--index',
                        type=int,
                        help='the simulation checkpoint index to use')

# Construct observe command parser
# =============================================================================
def setup_observe_parser(parser):
    parser.add_argument('obspath',
                        type=str,
                        help='the observation path')

    parser.add_argument('--force',
                        default=False,
                        action='store_true',
                        help='overwrite any existing files')

    parser.add_argument('--system',
                        type=str,
                        help='the system configuration to use')
    default = f'alma'
    parser.add_argument('--config',
                        type=str,
                        default=default,
                        help=f'the observing configuration to use [DEFAULT {default}]')

# Dust commands
# =============================================================================
def cmd_dust_define(args):
    defaults = None
    if args.copy is not None:
        defaults = dust_db.load(args.copy)

    # Create a dust object
    dust = Dust(
        name = args.name,
        desc = args.desc,
        min_size = args.min_size,
        max_size = args.max_size,
        power_index = args.power_index,
        material = args.material,
        porosity = args.porosity,
        grain_rho = args.grain_rho,
        defaults = defaults,
    )

    dust_db.save(dust)

    print(dust)

def cmd_dust_list(args):
    headers = ["Name", "MinSize\n(um)", "MaxSize\n(um)", "PowerLaw", "Material", "Porosity", "Density\n(g/cm3)"]

    table = []
    for dust_name in sorted(dust_db.list()):
        matches = len(args.name) == 0
        for pat in args.name:
            if fnmatch.fnmatch(dust_name, pat):
                matches = True
                break

        if matches:
            dust = dust_db.load(dust_name)
            row = [
                dust.name,
                dust.min_size.to('um').value, 
                dust.max_size.to('um').value, 
                dust.power_index,
                dust.material[:24],
                dust.porosity,
                dust.grain_rho.to('g/cm3').value,
            ]
            table.append(row)

    print(tabulate(table, headers=headers))

def cmd_dust_show(args):
    for dust_name in sorted(dust_db.list()):
        for pat in args.name:
            if fnmatch.fnmatch(dust_name, pat):
                dust = dust_db.load(dust_name)
                print(dust)
                print()
                break

def cmd_dust_remove(args):
    for dust_name in sorted(dust_db.list()):
        for pat in args.name:
            if fnmatch.fnmatch(dust_name, pat):
                if args.dry_run:
                    print(f'Would remove {dust_name}')
                else:
                    print(f'Removing {dust_name}')
                    dust = dust_db.delete(dust_name)
                break

# Disc commands
# =============================================================================
def cmd_disc_define(args):
    defaults = None
    if args.copy is not None:
        defaults = disc_db.load(args.copy)

    # Extract more complex arguments
    if args.aspect is not None:
        values = args.aspect.split(',')
        if len(values) == 1:
            aspect_ratio, aspect_flaring = values[0], None
        elif len(values) == 2:
            aspect_ratio, aspect_flaring = values[0], values[1]
        else:
            raise ValueError('')
    else:
        aspect_ratio, aspect_flaring = None, None

    if args.edges is not None:
        values = args.edges.split(',')
        if len(values) == 1:
            inner_edge, outer_edge = values[0], None
        elif len(values) == 2:
            inner_edge, outer_edge = values[0], values[1]
        else:
            raise ValueError('')
    else:
        inner_edge, outer_edge = None, None

    if args.sigma is not None:
        values = args.sigma.split(',')
        if len(values) == 1:
            sigma0, sigma_slope = values[0], None
        elif len(values) == 2:
            sigma0, sigma_slope = values[0], values[1]
        else:
            raise ValueError('')
    else:
        sigma0, sigma_slope = None, None

    if args.range_mstar is not None:
        values = args.range_mstar.split(',')
        if len(values) == 1:
            range_mstar_min = range_mstar_max = values[0]
        elif len(values) == 2:
            range_mstar_min, range_mstar_max = values[0], values[1]
        else:
            raise ValueError('')
    else:
        range_mstar_min, range_mstar_max = None, None

    if args.range_sma is not None:
        values = args.range_sma.split(',')
        if len(values) == 1:
            range_sma_min = range_sma_max = values[0]
        elif len(values) == 2:
            range_sma_min, range_sma_max = values[0], values[1]
        else:
            raise ValueError('')
    else:
        range_sma_min, range_sma_max = None, None

    # Create a disc object
    disc = Disc(
        name = args.name,
        desc = args.desc,
        units = args.units,
        mstar = args.mstar,
        aspect_ratio = aspect_ratio,
        aspect_flaring = aspect_flaring,
        inner_edge = inner_edge,
        outer_edge = outer_edge,
        sigma0 = sigma0,
        sigma_slope = sigma_slope,
        dust_ratio = args.dust_ratio,
        alpha = args.alpha,
        planet_mass = args.planet_mass,
        planet_sma = args.planet_sma,
        range_mstar_min = range_mstar_min,
        range_mstar_max = range_mstar_max,
        range_sma_min = range_sma_min,
        range_sma_max = range_sma_max,
        defaults = defaults,
    )

    disc_db.save(disc)

    print(disc)

def cmd_disc_list(args):
    headers = ["Name", "Units", "Aspect\nRatio", "Aspect\nFlare", "Sigma0\n\n[g/cm2]", "Sigma\nSlope", "Alpha", "Planet\nMass\n[Mp/M*]", "Inner\nEdge\n[AU]", "Outer\nEdge\n[AU]", "Stellar\nMass\n[Msun]", "Planet\nSMA\n[AU]"]

    table = []
    for disc_name in sorted(disc_db.list()):
        matches = len(args.name) == 0
        for pat in args.name:
            if fnmatch.fnmatch(disc_name, pat):
                matches = True
                break

        if matches:
            disc = disc_db.load(disc_name)
            if disc.units == DiscUnits.SCALE_FREE:
                length_unit = mass_unit = ''
            else:
                length_unit = 'AU'
                mass_unit = 'Msun'

            row = [
                disc.name,
                disc.units.name,
                disc.aspect_ratio,
                disc.aspect_flaring,
                disc.sigma0.value,
                disc.sigma_slope,
                disc.alpha,
                (disc.planet_mass / disc.mstar).value,
                disc.inner_edge.to(length_unit).value,
                disc.outer_edge.to(length_unit).value,
                disc.mstar.to(mass_unit).value,
                disc.planet_sma.to(length_unit).value,
            ]
            table.append(row)

    print(tabulate(table, headers=headers))

def cmd_disc_show(args):
    for disc_name in sorted(disc_db.list()):
        for pat in args.name:
            if fnmatch.fnmatch(disc_name, pat):
                disc = disc_db.load(disc_name)
                print(disc)
                print()
                break

def cmd_disc_remove(args):
    for disc_name in sorted(disc_db.list()):
        for pat in args.name:
            if fnmatch.fnmatch(disc_name, pat):
                if args.dry_run:
                    print(f'Would remove {disc_name}')
                else:
                    print(f'Removing {disc_name}')
                    disc = disc_db.delete(disc_name)
                break

# Hydro commands
# =============================================================================
def cmd_hydro_define(args):
    defaults = None
    if args.copy is not None:
        defaults = hydro_db.load(args.copy)

    # Extract and validate arguments
    if args.mesh_explicit:
        values = args.mesh_explicit.split(',')
        if len(values) == 2:
            mesh_azimuthal, mesh_radial = values[0], values[1]
        else:
            raise ValueError('')
    else:
        mesh_azimuthal, mesh_radial = None, None

    # Create a hydro object
    hydro = Hydro(
        name = args.name,
        desc = args.desc,
        preset = args.preset,
        mesh_cpsh = args.mesh_cpsh,
        mesh_azimuthal = mesh_azimuthal,
        mesh_radial = mesh_radial,
        max_fluids = args.max_fluids,
        min_edge = args.min_edge,
        orbits = args.orbits,
        checkpoint = args.checkpoint,
        defaults = defaults,
    )

    hydro_db.save(hydro)

    print(hydro)

def cmd_hydro_list(args):
    headers = ["Name", "Preset", "Mesh\n(CPSH)", "Mesh Az\n(Cells)", "Mesh Rad\n(Cells)", "Total\nOrbits", "Checkpoint\nOrbits", "Max\nFluids", "Min\nEdge"]

    table = []
    for hydro_name in sorted(hydro_db.list()):
        matches = len(args.name) == 0
        for pat in args.name:
            if fnmatch.fnmatch(hydro_name, pat):
                matches = True
                break

        if matches:
            hydro = hydro_db.load(hydro_name)
            row = [
                hydro.name,
                hydro.preset.name,
                hydro.mesh_cpsh,
                hydro.mesh_azimuthal,
                hydro.mesh_radial,
                hydro.orbits,
                hydro.checkpoint,
                hydro.max_fluids,
                hydro.min_edge,
            ]
            table.append(row)

    print(tabulate(table, headers=headers))

def cmd_hydro_show(args):
    for hydro_name in sorted(hydro_db.list()):
        for pat in args.name:
            if fnmatch.fnmatch(hydro_name, pat):
                hydro = hydro_db.load(hydro_name)
                print(hydro)
                print()
                break

def cmd_hydro_remove(args):
    for hydro_name in sorted(hydro_db.list()):
        for pat in args.name:
            if fnmatch.fnmatch(hydro_name, pat):
                if args.dry_run:
                    print(f'Would remove {hydro_name}')
                else:
                    print(f'Removing {hydro_name}')
                    hydro = hydro_db.delete(hydro_name)
                break

# System commands
# =============================================================================
def cmd_system_define(args):
    defaults = None
    if args.copy is not None:
        defaults = system_db.load(args.copy)

    # Create a system object
    system = System(
        name = args.name,
        desc = args.desc,
        distance = args.distance,
        coord = args.coord,
        incl = args.incl,
        posang = args.posang,
        mstar = args.mstar,
        rstar = args.rstar,
        tstar = args.tstar,
        planet_sma = args.planet_sma,
        defaults = defaults,
    )

    system_db.save(system)

    print(system)

def cmd_system_list(args):
    headers = ["Name", "Distance\n(pc)", "MStar\n(Msun)", "RStar\n(Rsun)", "TStar\n(K)", "Planet\nSMA (AU)", "Coord\n(RA DEC)", "Incl\n(deg)", "PosAng\n(deg)"]

    table = []
    for system_name in sorted(system_db.list()):
        matches = len(args.name) == 0
        for pat in args.name:
            if fnmatch.fnmatch(system_name, pat):
                matches = True
                break

        if matches:
            system = system_db.load(system_name)
            row = [
                system.name,
                system.distance.to('pc').value,
                system.mstar.to('Msun').value, 
                system.rstar.to('Rsun').value, 
                system.tstar.to('K').value, 
                system.planet_sma.to('AU').value, 
                system.coord,
                system.incl,
                system.posang,
            ]
            table.append(row)

    print(tabulate(table, headers=headers))

def cmd_system_show(args):
    for system_name in sorted(system_db.list()):
        for pat in args.name:
            if fnmatch.fnmatch(system_name, pat):
                system = system_db.load(system_name)
                print(system)
                print()
                break

def cmd_system_remove(args):
    for system_name in sorted(system_db.list()):
        for pat in args.name:
            if fnmatch.fnmatch(system_name, pat):
                if args.dry_run:
                    print(f'Would remove {system_name}')
                else:
                    print(f'Removing {system_name}')
                    system_= system_db.delete(system_name)
                break

# Simulate commands
# =============================================================================
def cmd_simulate(args):
    dust_name = args.dust
    disc_name = args.disc
    hydro_name = args.hydro

    dust = None if args.dust is None else dust_db.load(dust_name)
    disc = disc_db.load(disc_name)
    hydro = hydro_db.load(hydro_name)

    simpath = args.simpath
    if not os.path.isabs(simpath):
        simpath = os.path.join(VILYA_RESULTS_PATH, simpath)
    simpath = os.path.abspath(simpath)

    fcfg = FargoConfig(disc=disc, hydro=hydro, dust=dust)
    fcfg.make_install(path=simpath, overwrite=args.force)

# Radiative Transfer commands
# =============================================================================
def cmd_radiate(args):
    system_name = args.system
    dust_name = args.dust

    system = system_db.load(system_name)
    dust = None if args.dust is None else dust_db.load(dust_name)

    radpath = args.radpath
    if not os.path.isabs(radpath):
        radpath = os.path.join(VILYA_RESULTS_PATH, radpath)
    radpath = os.path.abspath(radpath)

    RadmcConfig.generate_makefile(system=system, dust=dust, index=args.index, radpath=radpath, overwrite=args.force)

# Observe commands
# =============================================================================
def cmd_observe(args):
    system_name = args.system

    system = system_db.load(system_name)

    obspath = args.obspath
    if not os.path.isabs(obspath):
        obspath = os.path.join(VILYA_RESULTS_PATH, obspath)
    obspath = os.path.abspath(obspath)

    CasaConfig.generate_makefile(system=system, config=args.config, obspath=obspath, overwrite=args.force)

# Command function mappings
# =============================================================================
COMMAND_FUNCTIONS = {
    ('dust', 'define'): cmd_dust_define,
    ('dust', 'list'): cmd_dust_list,
    ('dust', 'show'): cmd_dust_show,
    ('dust', 'remove'): cmd_dust_remove,

    ('disc', 'define'): cmd_disc_define,
    ('disc', 'list'): cmd_disc_list,
    ('disc', 'show'): cmd_disc_show,
    ('disc', 'remove'): cmd_disc_remove,

    ('hydro', 'define'): cmd_hydro_define,
    ('hydro', 'list'): cmd_hydro_list,
    ('hydro', 'show'): cmd_hydro_show,
    ('hydro', 'remove'): cmd_hydro_remove,

    ('system', 'define'): cmd_system_define,
    ('system', 'list'): cmd_system_list,
    ('system', 'show'): cmd_system_show,
    ('system', 'remove'): cmd_system_remove,

    ('simulate', None): cmd_simulate,
    ('radiate', None): cmd_radiate,
    ('observe', None): cmd_observe,
}

# Execute a command line
# =============================================================================
def execute():
    parser = setup_parser()
    args = parser.parse_args()
    command = (args.subparser1, None)
    if command not in COMMAND_FUNCTIONS:
        command = (args.subparser1, args.subparser2)
    function = COMMAND_FUNCTIONS[command]
    function(args)

# Main entry point when invoking as a module
# =============================================================================
if __name__ == '__main__':
    execute()
