# -*- coding: utf-8 -*-

import numpy as np

from astropy.units import Quantity, Unit

MATERIAL_DENSITY = {
    'astrosil': Quantity('3.3 g/cm3'),
    'c-gra': Quantity('2.16 g/cm3'),
    'c-nano': Quantity('2.3 g/cm3'),
    'c-org': Quantity('1.5 g/cm3'),
    'c-p': Quantity('1.8 g/cm3'),
    'c-z': Quantity('1.8 g/cm3'),
    'ch3oh-a': Quantity('0.779 g/cm3'),
    'ch3oh-c': Quantity('1.02 g/cm3'),
    'ch4-a': Quantity('0.47 g/cm3'),
    'ch4-c': Quantity('0.47 g/cm3'),
    'co-a': Quantity('0.81 g/cm3'),
    'co2-a': Quantity('1.2 g/cm3'),
    'co2-c': Quantity('1.67 g/cm3'),
    'co2-w': Quantity('1.6 g/cm3'),
    'cor-c': Quantity('4.0 g/cm3'),
    'fe-c': Quantity('7.87 g/cm3'),
    'fes': Quantity('4.83 g/cm3'),
    'h2o-a': Quantity('0.92 g/cm3'),
    'h2o-w': Quantity('0.92 g/cm3'),
    'nh3-m': Quantity('0.75 g/cm3'),
    'ol-c-mg00': Quantity('4.39 g/cm3'),
    'ol-c-mg100': Quantity('3.27 g/cm3'),
    'ol-c-mg95': Quantity('3.33 g/cm3'),
    'ol-mg40': Quantity('3.71 g/cm3'),
    'ol-mg50': Quantity('3.71 g/cm3'),
    'pyr-c-mg96': Quantity('2.80 g/cm3'),
    'pyr-mg100': Quantity('2.71 g/cm3'),
    'pyr-mg40': Quantity('3.3 g/cm3'),
    'pyr-mg50': Quantity('3.2 g/cm3'),
    'pyr-mg60': Quantity('3.1 g/cm3'),
    'pyr-mg70': Quantity('3.01 g/cm3'),
    'pyr-mg80': Quantity('2.9 g/cm3'),
    'pyr-mg95': Quantity('2.74 g/cm3'),
    'sic': Quantity('3.22 g/cm3'),
    'sio2': Quantity('2.65 g/cm3'),
}

COMPOSITE_MATERIALS = {
    'diana': ('pyr-mg70=0.870,c-z=0.130', 0.25),
    'dsharp': ('astrosil=0.329,c-org=0.397,fes=0.074,h2o-w=0.200', 0.0),
}

def lookup_material(material, porosity):
    material, porosity = COMPOSITE_MATERIALS.get(material, (material, porosity))
    return material, porosity

def compute_grain_rho(material, porosity):
    # Parse the materials
    total_mfrac = 0.0
    total_vfrac = 0.0
    for mat_mfrac in material.split(','):
        mat, mat_mfrac = [x.strip() for x in mat_mfrac.split('=')]

        mat_rho = MATERIAL_DENSITY[mat]
        mat_mfrac = float(mat_mfrac)
        mat_vfrac = mat_mfrac / mat_rho

        total_mfrac += mat_mfrac
        total_vfrac += mat_vfrac

    # Normalise
    total_vfrac /= total_mfrac
    porosity_vfrac = total_vfrac * (1 / (1 - porosity) - 1)

    # Compute the overall rho of the material
    grain_rho = 1 / (total_vfrac + porosity_vfrac) 

    return grain_rho
