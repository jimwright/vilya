# -*- coding: utf-8 -*-

def parse_value(value, fallback, defaults=None, name=None, ctor=str):
    if value is None and defaults is not None:
        # Perhaps also something smart here with named parameter sets?
        try:
            value = defaults.__getattribute__(name)
        except AttributeError:
            pass
        try:
            value = defaults[name]
        except (TypeError, KeyError):
            pass
                
    if value is None:
        value = fallback

    if value in ('', 'none', 'None', None):
        value = None
    else:
        value = ctor(value)

    return value
