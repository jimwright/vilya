# -*- coding: utf-8 -*-

from enum import Enum, unique
import glob
import json
import os
import re
import textwrap

import astropy.constants as apc
import astropy.units as apu

from .utils import parse_value
from . import VILYA_DEFNS_PATH

@unique
class HydroPreset(Enum):
    TINY = 1
    SMALL = 2
    MEDIUM = 3
    LARGE = 4
    HUGE = 5

    @classmethod
    def from_any(cls, value):
        try:
            # Returns for instances of the enum itself as well as any
            # matching integer values.
            return cls(value)
        except ValueError:
            try:
                # Returns for matching names
                return cls[str(value).upper()]
            except KeyError:
                raise ValueError(f'Cannot parse {value!r} as valid {cls}')

class HydroDatabase:
    # Construction
    # =========================================================================
    def __init__(self, path=os.path.join(VILYA_DEFNS_PATH, 'hydro')):
        self._path = path

    def save(self, hydro, overwrite=False):
        filename = self._make_filename(hydro.name)
        if os.path.exists(filename) and not overwrite:
            raise FileExistsError(filename)
        json_string = hydro.to_json()
        with open(filename, 'w') as file:
            file.write(json_string + '\n')

    def load(self, name):
        filename = self._make_filename(name)
        with open(filename) as file:
            json_string = ''.join(file.readlines())
        return Hydro.from_json(json_string)

    def delete(self, name):
        filename = self._make_filename(name)
        os.remove(filename)

    def list(self):
        result = []
        for filename in glob.glob(self._make_filename('*', validate=False)):
            hydro_name = os.path.splitext(os.path.basename(filename))[0]
            result.append(hydro_name)
        return result

    # Validation
    # =========================================================================
    @classmethod
    def validate_name(cls, name):
        # Needs to be something that is valid in the filing system
        if not re.match('^[0-9A-Za-z_:=,@+-]+$', name):
            raise ValueError(f'hydro name {name} contains an invalid character')
        
    # Helper functions
    # =========================================================================
    def _make_filename(self, hydro_name, validate=True):
        if validate:
            self.validate_name(hydro_name)
        return os.path.join(self._path, hydro_name + '.json')

class Hydro:
    DEFAULT_PRESET = HydroPreset.SMALL

    DEFAULT_CPSH = 8.00
    PRESET_CPSH = {
        HydroPreset.TINY: 4.00,
        HydroPreset.SMALL: 5.66,
        HydroPreset.MEDIUM: 8.00,
        HydroPreset.LARGE: 11.31,
        HydroPreset.HUGE: 16.00,
    }

    DEFAULT_MESH_AZIMUTHAL = None
    DEFAULT_MESH_RADIAL = None

    DEFAULT_ORBITS = 500
    PRESET_ORBITS = {
        HydroPreset.TINY: 25,
        HydroPreset.SMALL: 125,
        HydroPreset.MEDIUM: 500,
        HydroPreset.LARGE: 1000,
        HydroPreset.HUGE: 1000,
    }

    DEFAULT_CHECKPOINT = 100
    PRESET_CHECKPOINT = {
        HydroPreset.TINY: 5,
        HydroPreset.SMALL: 25,
        HydroPreset.MEDIUM: 50,
        HydroPreset.LARGE: 100,
        HydroPreset.HUGE: 100,
    }

    DEFAULT_MAX_FLUIDS = 6
    PRESET_MAX_FLUIDS = {
        HydroPreset.TINY: 2,
        HydroPreset.SMALL: 3,
        HydroPreset.MEDIUM: 4,
        HydroPreset.LARGE: 5,
        HydroPreset.HUGE: 6,
    }

    DEFAULT_MIN_EDGE = 0.1
    PRESET_MIN_EDGE = {
        HydroPreset.TINY: 0.400,
        HydroPreset.SMALL: 0.283,
        HydroPreset.MEDIUM: 0.200,
        HydroPreset.LARGE: 0.141,
        HydroPreset.HUGE: 0.100,
    }

    # Construction
    # =========================================================================
    def __init__(self,
                 name = None,
                 desc = None,
                 preset = None,
                 mesh_cpsh = None,
                 mesh_azimuthal = None,
                 mesh_radial = None,
                 max_fluids = None,
                 min_edge = None,
                 orbits = None,
                 checkpoint = None,
                 defaults = None):
        # Descriptive properties
        name, desc = self._parse_descriptive(
            name = name,
            desc = desc,
            defaults = defaults)

        self._name = name
        self._desc = desc

        # Preset properties
        preset = self._parse_preset(
            preset = preset,
            defaults = defaults)
        self._preset = preset

        # Mesh properties
        mesh_cpsh, mesh_azimuthal, mesh_radial = self._parse_mesh(
            mesh_cpsh = mesh_cpsh,
            mesh_azimuthal = mesh_azimuthal,
            mesh_radial = mesh_radial,
            preset = preset,
            defaults = defaults)

        self._mesh_cpsh = mesh_cpsh
        self._mesh_azimuthal = mesh_azimuthal
        self._mesh_radial = mesh_radial

        # Limiting properties
        max_fluids, min_edge = self._parse_limits(
            max_fluids = max_fluids,
            min_edge = min_edge,
            preset = preset,
            defaults = defaults)

        self._max_fluids = max_fluids
        self._min_edge = min_edge

        # Oribit properties
        orbits, checkpoint = self._parse_orbits(
            orbits = orbits,
            checkpoint = checkpoint,
            preset = preset,
            defaults = defaults)

        self._orbits = orbits
        self._checkpoint = checkpoint

    @classmethod
    def _parse_descriptive(cls, name, desc, defaults=None):
        name = parse_value(name, None, defaults, 'name', ctor=str)
        desc = parse_value(desc, None, defaults, 'desc', ctor=str)

        HydroDatabase.validate_name(name)

        return name, desc

    @classmethod
    def _parse_preset(cls, preset, defaults):
        preset = parse_value(preset, cls.DEFAULT_PRESET, defaults, 'preset', ctor=HydroPreset.from_any)

        return preset

    @classmethod
    def _parse_mesh(cls, mesh_cpsh, mesh_azimuthal, mesh_radial, preset, defaults):
        default_cpsh = cls.PRESET_CPSH.get(preset, cls.DEFAULT_CPSH)

        mesh_cpsh = parse_value(mesh_cpsh, default_cpsh, defaults, 'mesh_cpsh', ctor=float)
        mesh_azimuthal = parse_value(mesh_azimuthal, cls.DEFAULT_MESH_AZIMUTHAL, defaults, 'mesh_azimuthal', ctor=int)
        mesh_radial = parse_value(mesh_radial, cls.DEFAULT_MESH_RADIAL, defaults, 'mesh_radial', ctor=int)

        return mesh_cpsh, mesh_azimuthal, mesh_radial

    @classmethod
    def _parse_limits(cls, max_fluids, min_edge, preset, defaults):
        default_max_fluids = cls.PRESET_MAX_FLUIDS.get(preset, cls.DEFAULT_MAX_FLUIDS)
        default_min_edge = cls.PRESET_MIN_EDGE.get(preset, cls.DEFAULT_MIN_EDGE)

        max_fluids = parse_value(max_fluids, default_max_fluids, defaults, 'max_fluids', ctor=int)
        min_edge = parse_value(min_edge, default_min_edge, defaults, 'min_edge', ctor=float)

        return max_fluids, min_edge

    @classmethod
    def _parse_orbits(cls, orbits, checkpoint, preset, defaults):
        default_orbits = cls.PRESET_ORBITS.get(preset, cls.DEFAULT_ORBITS)
        default_checkpoint = cls.PRESET_CHECKPOINT.get(preset, cls.DEFAULT_CHECKPOINT)

        orbits = parse_value(orbits, default_orbits, defaults, 'orbits', ctor=int)
        checkpoint = parse_value(checkpoint, default_checkpoint, defaults, 'checkpoint', ctor=int)

        return orbits, checkpoint

    # Descriptive properties
    # =========================================================================
    @property
    def name(self):
        return self._name

    @property
    def desc(self):
        return self._desc

    # Mesh properties
    # =========================================================================
    @property
    def preset(self):
        return self._preset

    @property
    def mesh_cpsh(self):
        return self._mesh_cpsh

    @property
    def mesh_azimuthal(self):
        return self._mesh_azimuthal

    @property
    def mesh_radial(self):
        return self._mesh_radial

    # Limit properties
    # =========================================================================
    @property
    def max_fluids(self):
        return self._max_fluids

    @property
    def min_edge(self):
        return self._min_edge

    # Orbit properties
    # =========================================================================
    @property
    def orbits(self):
        return self._orbits

    @property
    def checkpoint(self):
        return self._checkpoint

    # Formatting methods
    # =========================================================================
    def __str__(self):
        return textwrap.dedent(f'''\
            NAME:   {self.name}
            DESC:   {self.desc}
            PRESET: {self.preset.name}
            MESH:   cpsh={self.mesh_cpsh}, explict={self.mesh_azimuthal},{self.mesh_radial}
            LIMITS: max_fluids={self.max_fluids}, min_edge={self.min_edge}
            ORBITS: {self.orbits}, with checkpoint every {self.checkpoint}''')

    def __repr__(self):
        return (f'{self.__class__.__name__}('
            f'name={self.name!r}, '
            f'desc={self.desc!r}, '
            f'preset={self.preset.name!r}, '
            f'mesh_cpsh={self.mesh_cpsh!r}, '
            f'mesh_azimuthal={self.mesh_azimuthal!r}, '
            f'mesh_radial={self.mesh_radial!r}, '
            f'max_fluids={self.max_fluids!r}, '
            f'min_edge={self.min_edge!r}, '
            f'orbits={self.orbits!r}, '
            f'checkpoint={self.checkpoint!r})')

    # Serialisation methods
    # =========================================================================
    def to_json(self):
        return json.dumps({
            'version': 1,
            'name': self.name,
            'desc': self.desc,
            'preset': self.preset.name,
            'mesh_cpsh': self.mesh_cpsh,
            'mesh_azimuthal': self.mesh_azimuthal,
            'mesh_radial': self.mesh_radial,
            'max_fluids': self.max_fluids,
            'min_edge': self.min_edge,
            'orbits': self.orbits,
            'checkpoint': self.checkpoint,
        })

    @classmethod
    def from_json(cls, value):
        kwrds = json.loads(value)
        version = kwrds.pop('version')
        return cls(**kwrds)

    # Computed properties
    # =========================================================================
