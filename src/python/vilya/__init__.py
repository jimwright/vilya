# -*- coding: utf-8 -*-

import os

def find_venv_path():
    path = os.path.normpath(__file__)
    path, prev_path = os.path.dirname(path), path
    while not os.path.isfile(os.path.join(path, 'pyvenv.cfg')) and path != prev_path:
        path, prev_path = os.path.dirname(path), path
    if path == prev_path:
        raise FileNotFoundError('Cannot find pyveng.cfg for virtual environment')
    return path

VILYA_VENV_PATH = find_venv_path()
VILYA_BIN_PATH = os.path.join(VILYA_VENV_PATH, 'bin')
VILYA_PATH = os.path.dirname(VILYA_VENV_PATH)
VILYA_DEFNS_PATH = os.path.join(VILYA_PATH, 'defns')
VILYA_RESULTS_PATH = os.path.join(VILYA_PATH, 'results')

del find_venv_path
del os
