# -*- coding: utf-8 -*-

import argparse
import os
import struct
import subprocess

import numpy as np
from scipy.stats import norm
from scipy.optimize import bisect

from astropy.units import Quantity

from . import VILYA_RESULTS_PATH
from .system import SystemDatabase
from .fargo import FargoFrame

# ==============================================================================

class RadmcConfig:
    DEFAULT_LOWER_LAMBDA = Quantity('0.1um')
    DEFAULT_UPPER_LAMBDA = Quantity('10mm')
    DEFAULT_NUM_LAMBDA = 500
    DEFAULT_NUM_THETA = 64

    def __init__(self,
            lower_lambda = DEFAULT_LOWER_LAMBDA,
            upper_lambda = DEFAULT_UPPER_LAMBDA,
            num_lambda = DEFAULT_NUM_LAMBDA,
            num_theta = DEFAULT_NUM_THETA,
            num_threads = os.cpu_count()-1):

        self.lower_lambda = Quantity(lower_lambda).to('um')
        self.upper_lambda = Quantity(upper_lambda).to('um')
        self.num_lambda = int(num_lambda)
        self.num_theta = int(num_theta)
        self.num_threads = max(1, int(num_threads))

    def make_theta_grid(self, frame):
        # Compute the number of standard deviations that would contain
        # 1/num_theta of the distribution within the interval +/- n_stdev for
        # a Gaussian distribution (which we know the gas vertical distribution
        # to be).
        n_stdev = norm.ppf(0.5 + 0.5 / self.num_theta)

        # Compute the value of the probability density function at n_stdev
        # assuming a Gaussian distribution.
        gaus_pdf = 1 / (2*np.pi)**0.5 * np.exp(-0.5 * (n_stdev/1)**2)

        # We wish to capture 99.9% of the disc, so take our maximum theta to
        # be four times the scale height.  Assume any flared disc that falls
        # outside of this to be negligable.
        theta_max = np.arctan(4.0 * frame.aspect_ratio)

        # Before iterating through the different dust species we need to
        # seed an initial value for the minimum theta that we will update.
        # May as well initialise to the maximum theta.
        theta_min = theta_max

        # Compute the gas pressure scale height at R = R0 as a convenience.
        height0 = frame.planet_sma * frame.aspect_ratio

        # Consider each of the dust species one by one and refine the
        # minimum theta.
        for dust in frame.dusts:
            # Define a function that will be used to search for the theta
            # value in the dust's vertical profile where the probability
            # density equals gaus_pdf.  We will assume that the interval
            # +/- theta will contain approximately 1/num_theta of the dust.
            # We justify this because the form of the dust's vertical profile
            # is largely a Gaussian modulated by a smooth monotonically
            # decreasing function (from 1 to 0), so we expect the resulting
            # distribution to be approximately Gaussian too.
            rhod_fn = lambda theta: (
                gaus_pdf - 1 / (2*np.pi)**0.5
                * np.exp(-0.5 * (frame.planet_sma * np.tan(theta) / height0)**2)
                * np.exp(-dust.stokes0/frame.alpha * (np.exp(0.5 * (frame.planet_sma * np.tan(theta) / height0)**2) - 1)))

            # Use bisection to find the value of theta_dust.
            theta_dust = bisect(rhod_fn, 0.0, theta_max)

            # Update theta_min if theta_dust is smaller than the current
            # minimum.  Thus by the end, theta_min will contain a value of
            # theta that 
            theta_min = min(theta_min, theta_dust)

        # Generate a log spacing starting at theta_min upto theta_max in
        # num_theta+1 steps.  These will become the edges of the cells
        # hence the +1 in steps, but will need some transformations to be
        # applied first.  Using a log spacing should provide sufficient
        # resolution to capture the vertical structure of the smallest dust
        # whilst also extending out to capture all of the smallest dust
        # that is dynamically coupled to the gas.
        theta_edge = np.logspace(np.log10(theta_min), np.log10(theta_max), self.num_theta+1)

        # Radmc needs the angles to start on the outside of the disc and
        # move towards the midplane which is at np.pi/2.  So we first need
        # to reverse the order of the list.
        theta_edge = theta_edge[::-1]

        # Now we linearly transform the grid so that theta_min = 0.0 and
        # theta_max is unchanged.  We couldn't do that to start with as we
        # cannot start a log spacing from 0.0.  These now really are the
        # edges of the cells.
        theta_edge = (theta_edge - theta_edge[-1]) * theta_edge[0] / (theta_edge[0] - theta_edge[-1])

        # Finally do the reflection across the theta = 45 deg line, so that
        # theta_min = pi/2.
        theta_edge = 0.5 * np.pi - theta_edge

        # Populate the frame object with the theta grid for convenience since it
        # already contains the r and phi grids.
        frame.theta_edge = Quantity(theta_edge)
        frame.theta_centre = 0.5 * (frame.theta_edge[:-1] + frame.theta_edge[1:])
        frame.num_theta = len(frame.theta_centre)

    def make_densities(self, frame):
        # Grab some values that will be useful later on
        r_centre = frame.r_centre           # Radial centres of the grid
        height_centre = frame.height_centre # Radial gas pressure scale heights
        gas_sigma = frame.sigma['gas']      # 2D gas surface density

        # In order to avoid an unphysically hot and bright rim of the inner
        # disc Radmc3d recommends refining (making smaller) the radial grid
        # on the inner edge of the disc.  This causes a problem because the
        # setup is not physically realistic as normally there would be
        # additional disc between the star and our artifical inner edge.
        # The refinement reduces the optical depth of those cells closest
        # to the star, however here we reduce the density of the cells in the
        # damping zone which also has the effect of reducing the optical depth.
        # In any case given the damping zone contains non physical effects
        # reducing its visibility is here is a win-win.
        radial_damping = (frame.r_centre - frame.inner_edge) / (frame.inner_edge * (frame.damping_zone - 1))
        radial_damping = np.clip(radial_damping, 0.0, 1.0)[:, np.newaxis, np.newaxis]

        # Compute the vertical height of each cell in the grid
        cell_height = 1/np.tan(frame.theta_edge[:-1].value) - 1/np.tan(frame.theta_edge[1:].value)
        cell_height = (r_centre[:, np.newaxis, np.newaxis] * cell_height[np.newaxis, :, np.newaxis]).to('cm')

        # Compute the different dust densities one by one
        densities = {}
        for dust in frame.dusts:
            # Initialise the dust distribution grid to zeros
            distrib = np.zeros(shape=(frame.num_r, frame.num_theta, frame.num_phi), dtype='f8')

            # Compute the stokes number for this fluid at the mid-plane
            stokes_mid = dust.stokes0 * frame.sigma0 / gas_sigma

            # Get the dust surface density grid
            dust_sigma = frame.sigma[dust.name].to('g/cm2')

            # Fill in the dust density expected at the mid point of each
            # vertical cell.  We don't assume any particular density at
            # the midplane for now, but will correct for this later.
            for it, theta in enumerate(frame.theta_centre):
                z = r_centre / np.tan(theta.value)
                gamma = 0.5 * (z**2 / height_centre**2)[:, np.newaxis]
                distrib[:, it, :] = np.exp(-stokes_mid/frame.alpha * (np.exp(gamma) - 1) - gamma)

            # Compute the surface density for each vertical layer
            distrib = distrib * cell_height

            # Get the total surface density, remembering that we are have
            # only calculated above the mid-plane, so need to double.
            surface_density = 2 * np.sum(distrib, axis=1)

            # Normalise the distribution so that the surface
            distrib *= (dust_sigma / surface_density)[:, np.newaxis, :]

            # Convert distribution back from a surface density to the volume
            # density and apply the radial damping described above.
            densities[dust.name] = (distrib / cell_height).to('g/cm3') * radial_damping

        return densities

    def make_spectra(self):
        spectra = np.logspace(
            np.log10(self.lower_lambda.to('um').value),
            np.log10(self.upper_lambda.to('um').value),
            self.num_lambda,
            endpoint=True)
        return spectra

    def write_stars_and_wavelengths(self, path, system, spectra):
        with open(os.path.join(path, 'wavelength_micron.inp'), 'w') as f:
            f.write(f'{len(spectra)}\n')    # Number of wavelengths in spectra
            for lambda_value in spectra:
                f.write(f'{lambda_value}\n')

        with open(os.path.join(path, 'stars.inp'), 'w') as f:
            # Convert values to appropriate radmc units
            radius = system.rstar.to('cm').value
            mass = system.mstar.to('g').value
            temp = system.tstar.to('K').value

            f.write(f'2\n')                 # Format number
            f.write(f'1 {len(spectra)}\n')  # Single star, number of wavelengths in spectra
            f.write(f'{radius} {mass} 0.0 0.0 0.0\n') # Radius, mass, xyz position
            f.write(f'\n')

            for lambda_value in spectra:
                f.write(f'{lambda_value}\n')
            f.write(f'\n')

            f.write(f'{-temp}\n')           # Use blackbody radiation field for star

    def write_grid(self, path, frame):
        with open(os.path.join(path, 'amr_grid.inp'), 'w') as f:
            f.write(f'1\n')                 # Format number
            f.write(f'0\n')                 # AMR grid style 0=regular
            f.write(f'100\n')               # Coordinate system 100=spherical
            f.write(f'0\n')                 # Grid info
            f.write(f'1 1 1\n')             # Including r, theta, phi coordinates
            f.write(f'{frame.num_r} {frame.num_theta} {frame.num_phi}\n') 
            for value in frame.r_edge:
                f.write(f'{value.to("cm").value:13.10e}\n') # Radial edges (walls)
            for value in frame.theta_edge:
                f.write(f'{value.value!r}\n')               # Vertical edges (walls)
            for value in frame.phi_edge:
                f.write(f'{value.value + np.pi!r}\n')       # Azimuthal edges (walls)

    def write_densities(self, path, frame, densities):
        with open(os.path.join(path, 'dust_density.binp'), 'wb') as f:
            f.write(struct.pack('q', 1))    # Format number
            f.write(struct.pack('q', 4))    # 4 byte floating point values used
            f.write(struct.pack('q', frame.num_r * frame.num_theta * frame.num_phi))
            f.write(struct.pack('q', frame.num_dusts))
            for dust in frame.dusts:
                data = densities[dust.name].to('g/cm3').value.ravel(order='F').astype(np.float32)
                data.tofile(f)

    def write_opacities(self, path, frame):
        power_index = frame.dust.power_index
        material = frame.dust.material
        porosity = frame.dust.porosity
        max_size = frame.dust.max_size

        for dust in frame.dusts:
            if not os.path.exists(os.path.join(path, f'dustkapscatmat_{dust.name}.inp')):
                microns_min = min(dust.min_size, max_size).to('um').value
                microns_max = min(dust.max_size, max_size).to('um').value

                command = [ 'optool' ]
                if material is not None:
                    for mat in material.split(','):
                        (mat_name, mat_mfrac) = mat.split('=')
                        command.extend(['-c', mat_name.strip(), mat_mfrac.strip()])
                if porosity is not None:
                    command.extend(['-p', str(porosity)])
                command += [
                    '-a', f'{microns_min}', f'{microns_max}', f'{power_index}',
                    '-l', 'wavelength_micron.inp',
                    '-s',
                    '-chop', '5',
                    '-radmc', f'{dust.name}'
                ]
                print(command)
                subprocess.run(args=command, cwd=path)

        with open(os.path.join(path, 'dustopac.inp'), 'w') as f:
            f.write(f'2\n')                     # Format number
            f.write(f'{frame.num_dusts}\n')    # Number of dust species
            for dust in frame.dusts:
                f.write(f'========\n')
                f.write(f'10\n')                # Way in which dust species is read
                f.write(f'0\n')                 # 0=Thermal grain
                f.write(f'{dust.name}\n')       # dustkapscatmat_XXX.inp extension

    def write_radmc_config(self, path, frame):
        # We will shoot 10 photos for every cell at 50 million cells, and scale this
        # proportionally to the square root ratio of the mesh sizes.
        mesh_size = len(frame.r_edge) * len(frame.theta_edge) * len(frame.phi_edge)
        num_thermal_photons = 10 * 50e6 * (mesh_size/50e6) ** 0.5
        num_image_photons = 10000000

        with open(os.path.join(path, 'radmc3d.inp'), 'w') as f:
            f.write(f'nphot_therm = {int(num_thermal_photons)}\n')
            f.write(f'nphot_scat = {int(num_image_photons)}\n')
            f.write(f'scattering_mode_max = 1\n')
            f.write(f'iranfreqmode = 1\n')
            f.write(f'setthreads = {self.num_threads}\n')
            f.write(f'rto_style = 3\n')
            f.write(f'rto_single = 1\n')
            f.write(f'istar_sphere = 1\n')

    def generate_config(self, frame, system, radpath=None, overwrite=False):
        # Apply defaults to output path
        if radpath is None:
            # Generate a name if none is given
            radpath = os.path.join(os.path.dirname(frame.path), f'{system.name}')
        if not os.path.isabs(radpath):
            # Root relative paths wrt the results directory, not cwd
            radpath = os.path.join(VILYA_RESULTS_PATH, radpath)
        # Form normalised absolute path
        radpath = os.path.abspath(radpath)
        # The result name will be the basename (last directory component)
        radname = os.path.basename(radpath)

        frame.set_scale(system.mstar, system.planet_sma)
        frame.dump()

        self.make_theta_grid(frame)
        densities = self.make_densities(frame)
        spectra = self.make_spectra()

        if not os.path.isdir(radpath):
            os.makedirs(radpath, exist_ok=overwrite)

        self.write_grid(radpath, frame)
        self.write_densities(radpath, frame, densities)
        self.write_stars_and_wavelengths(radpath, system, spectra)
        self.write_opacities(radpath, frame)
        self.write_radmc_config(radpath, frame)

    @classmethod
    def generate_makefile(self, system, dust=None, index=None, radpath=None, overwrite=False):
        if not os.path.isabs(radpath):
            # Root relative paths wrt the results directory, not cwd
            radpath = os.path.join(VILYA_RESULTS_PATH, radpath)
        # Form normalised absolute path
        radpath = os.path.abspath(radpath)
        # The result name will be the basename (last directory component)
        radname = os.path.basename(radpath)

        if not os.path.isdir(radpath):
            os.makedirs(radpath, exist_ok=overwrite)

        options = f'--system {system.name}'
        if dust is not None:
            options += f' --dust {dust.name}'
        if index is not None:
            options += f' --index {index}'

        with open(os.path.join(radpath, 'Makefile'), 'w') as f:
            f.write(f'''\
ALL_TARGETS += {radname}/dust_temperature.bdat

{radname}/dust_temperature.bdat: {radname}/dust_density.binp
\t@echo "### Running $(SIM_NAME)/{radname} radiative xfer using Radmc3D, this may take some time"
\t@echo "### NB: If interrupted Racmc3D will restart from the beginning"
\tcd {radname} && time -v radmc3d mctherm >>radmc3d-mctherm.log 2>&1

{radname}/dust_density.binp: $(FINAL_FARGO_SUMMARY)
\t@echo "### Importing Fargo3D simulation results ready for $(SIM_NAME)/{radname} radiative xfer"
\tcd {radname} && time -v python -m vilya.radmc {options} $(SIM_PATH)/{radname} >>radmc3d-import.log 2>&1
''')

def execute():
    parser = argparse.ArgumentParser()
    parser.add_argument('--system', type=str, metavar='SYSTEM_NAME')
    parser.add_argument('--dust', type=str, metavar='DUST_NAME')
    parser.add_argument('--index', type=int, metavar='CHECKPOINT')
    parser.add_argument('radpath', type=str, metavar='RADPATH')
    args = parser.parse_args()

    radpath = args.radpath
    simpath = os.path.join(args.radpath, '..')

    system_db = SystemDatabase()
    system = system_db.load(args.system)
    frame = FargoFrame(path=os.path.join(simpath, 'outputs'), dust=args.dust)
    frame.load_index(args.index)

    rcfg = RadmcConfig()
    rcfg.generate_config(frame, system, radpath=radpath)

if __name__ == '__main__':
    execute()
