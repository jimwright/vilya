# -*- coding: utf-8 -*-

from enum import Enum, unique
import glob
import json
import os
import re
import textwrap

import numpy as np

import astropy.constants as apc
import astropy.units as apu
from astropy.units import Quantity, Unit

from .utils import parse_value
from . import VILYA_DEFNS_PATH

@unique
class DiscUnits(Enum):
    SCALE_FREE = 1
    CGS = 2
    MKS = 3

    @classmethod
    def from_any(cls, value):
        try:
            # Returns for instances of the enum as well as any
            # matching integer values.
            return cls(value)
        except ValueError:
            try:
                # Returns for matching names
                return cls[str(value).upper()]
            except KeyError:
                raise ValueError(f'Cannot parse {value!r} as valid {cls}')

class DiscDatabase:
    # Construction
    # =========================================================================
    def __init__(self, path=os.path.join(VILYA_DEFNS_PATH, 'disc')):
        self._path = path

    def save(self, disc, overwrite=False):
        filename = self._make_filename(disc.name)
        if os.path.exists(filename) and not overwrite:
            raise FileExistsError(filename)
        json_string = disc.to_json()
        with open(filename, 'w') as file:
            file.write(json_string + '\n')

    def load(self, name):
        filename = self._make_filename(name)
        with open(filename) as file:
            json_string = ''.join(file.readlines())
        return Disc.from_json(json_string)

    def delete(self, name):
        filename = self._make_filename(name)
        os.remove(filename)

    def list(self):
        result = []
        for filename in glob.glob(self._make_filename('*', validate=False)):
            disc_name = os.path.splitext(os.path.basename(filename))[0]
            result.append(disc_name)
        return result

    # Validation
    # =========================================================================
    @classmethod
    def validate_name(cls, name):
        # Needs to be something that is valid in the filing system
        if not re.match('^[0-9A-Za-z_:=,@+-]+$', name):
            raise ValueError(f'Disc name {name} contains an invalid character')
        
    # Helper functions
    # =========================================================================
    def _make_filename(self, disc_name, validate=True):
        if validate:
            self.validate_name(disc_name)
        return os.path.join(self._path, disc_name + '.json')

class Disc:
    DEFAULT_UNITS = DiscUnits.SCALE_FREE
    DEFAULT_SCALEFREE_LENGTH = Quantity("1")
    DEFAULT_SCALEFREE_MSTAR = Quantity("1")
    DEFAULT_SCALE_LENGTH = Quantity("1 AU")
    DEFAULT_SCALE_MSTAR = Quantity("1 Msun")

    DEFAULT_ASPECT_RATIO = 0.07
    DEFAULT_ASPECT_FLARING = 0.25
    DEFAULT_INNER_EDGE = 0.4

    DEFAULT_SIGMA0 = 0.00333  # For scale free matches ~30g/cm2 at 31.6AU with M_sun
    DEFAULT_SIGMA_SLOPE = 1.0
    DEFAULT_DUST_RATIO = 0.01

    DEFAULT_ALPHA = 1e-3

    DEFAULT_PLANET_MASS = 1e-3      # Approx Jupiter mass
    DEFAULT_PLANET_SMA = 1.0

    DEFAULT_RANGE_MSTAR_MIN = Quantity('0.5 Msun')
    DEFAULT_RANGE_MSTAR_MAX = Quantity('2.0 Msun')
    DEFAULT_RANGE_SMA_MIN = Quantity('10 AU')
    DEFAULT_RANGE_SMA_MAX = Quantity('100 AU')

    # Construction
    # =========================================================================
    def __init__(self,
                 name = None,
                 desc = None,
                 units = None,
                 mstar = None,
                 aspect_ratio = None,
                 aspect_flaring = None,
                 inner_edge = None,
                 outer_edge = None,
                 sigma0 = None,
                 sigma_slope = None,
                 dust_ratio = None,
                 alpha = None,
                 planet_mass = None,
                 planet_sma = None,
                 range_mstar_min = None,
                 range_mstar_max = None,
                 range_sma_min = None,
                 range_sma_max = None,
                 defaults = None):

        # Descriptive properties
        name, desc = self._parse_descriptive(
            name = name,
            desc = desc,
            defaults = defaults)

        self._name = name
        self._desc = desc

        # Unit properties
        units, scale_length, mstar = self._parse_units(
            units = units,
            mstar = mstar,
            defaults = defaults)

        self._units = units
        self._mstar = mstar

        unit_length = scale_length.unit
        unit_mass = mstar.unit

        # Planet properties
        planet_mass, planet_sma = self._parse_planet(
            planet_mass = planet_mass,
            planet_sma = planet_sma,
            defaults = defaults)

        if planet_mass.unit.is_unity(): planet_mass *= mstar
        if planet_sma.unit.is_unity(): planet_sma *= scale_length

        self._planet_mass = planet_mass.to(unit_mass)
        self._planet_sma = planet_sma.to(unit_length)

        # Geometric properties
        aspect_ratio, aspect_flaring, inner_edge, outer_edge = self._parse_geometric(
            aspect_ratio = aspect_ratio,
            aspect_flaring = aspect_flaring,
            inner_edge = inner_edge,
            outer_edge = outer_edge,
            defaults = defaults)

        if inner_edge.unit.is_unity():
            inner_edge *= planet_sma
        if outer_edge is None:
            outer_edge = (planet_sma / inner_edge) * planet_sma
        elif outer_edge.unit.is_unity():
            outer_edge *= planet_sma

        self._aspect_ratio = aspect_ratio.to('').value
        self._aspect_flaring = aspect_flaring.to('').value
        self._inner_edge = inner_edge.to(unit_length)
        self._outer_edge = outer_edge.to(unit_length)

        # Composition properties
        sigma0, sigma_slope, dust_ratio = self._parse_composition(
            sigma0 = sigma0,
            sigma_slope = sigma_slope,
            dust_ratio = dust_ratio,
            defaults = defaults)

        if sigma0.unit.is_unity(): sigma0 *= unit_mass / unit_length**2

        self._sigma0 = sigma0.to(unit_mass / unit_length**2)
        self._sigma_slope = sigma_slope.to('').value
        self._dust_ratio = dust_ratio.to('').value

        # Kinematic properties
        alpha = self._parse_kinematic(
            alpha=alpha,
            defaults=defaults)

        self._alpha = alpha.to('').value

        # Scale free disc permitted ranges
        if not self.is_scale_free:
            if range_mstar_min is None: range_mstar_min = self.mstar
            if range_mstar_max is None: range_mstar_max = self.mstar
            if range_sma_min is None: range_sma_min = self.planet_sma
            if range_sma_max is None: range_sma_max = self.planet_sma

        range_mstar_min, range_mstar_max, range_sma_min, range_sma_max = self._parse_ranges(
            range_mstar_min = range_mstar_min,
            range_mstar_max = range_mstar_max,
            range_sma_min = range_sma_min,
            range_sma_max = range_sma_max,
            defaults = defaults)

        if range_mstar_min.unit.is_unity(): range_mstar_min *= Unit('Msun')
        if range_mstar_max.unit.is_unity(): range_mstar_max *= Unit('Msun')
        if range_sma_min.unit.is_unity(): range_sma_min *= Unit('AU')
        if range_sma_max.unit.is_unity(): range_sma_max *= Unit('AU')

        self._range_mstar_min = range_mstar_min.to('Msun')
        self._range_mstar_max = range_mstar_max.to('Msun')
        self._range_sma_min = range_sma_min.to('AU')
        self._range_sma_max = range_sma_max.to('AU')

    @classmethod
    def _parse_descriptive(cls, name, desc, defaults=None):
        name = parse_value(name, None, defaults, 'name', ctor=str)
        desc = parse_value(desc, None, defaults, 'desc', ctor=str)

        DiscDatabase.validate_name(name)

        return name, desc

    @classmethod
    def _parse_units(cls, units, mstar, defaults=None):
        units = parse_value(units, cls.DEFAULT_UNITS, defaults, 'units', ctor=DiscUnits.from_any)
        if units == DiscUnits.SCALE_FREE:
            length_unit, mass_unit = Unit(), Unit()
        elif units == DiscUnits.CGS:
            length_unit, mass_unit = Unit('cm'), Unit('g')
        elif units == DiscUnits.MKS:
            length_unit, mass_unit = Unit('m'), Unit('kg')

        # Scale masses and lengths
        if units == DiscUnits.SCALE_FREE:
            scale_length = cls.DEFAULT_SCALEFREE_LENGTH
            mstar = cls.DEFAULT_SCALEFREE_MSTAR
        else:
            scale_length = cls.DEFAULT_SCALE_LENGTH
            mstar = parse_value(mstar, cls.DEFAULT_SCALE_MSTAR, defaults, 'mstar', ctor=Quantity)

        if scale_length.unit.is_unity(): scale_length *= length_unit
        if mstar.unit.is_unity(): mstar *= mass_unit

        scale_length = scale_length.to(length_unit)
        mstar = mstar.to(mass_unit)

        return units, scale_length, mstar

    @classmethod
    def _parse_geometric(cls, aspect_ratio, aspect_flaring, inner_edge, outer_edge, defaults=None):
        # Geometric properties
        aspect_ratio = parse_value(aspect_ratio, cls.DEFAULT_ASPECT_RATIO, defaults, 'aspect_ratio', ctor=Quantity)
        aspect_flaring = parse_value(aspect_flaring, cls.DEFAULT_ASPECT_FLARING, defaults, 'aspect_flaring', ctor=Quantity)
        inner_edge = parse_value(inner_edge, cls.DEFAULT_INNER_EDGE, defaults, 'inner_edge', ctor=Quantity)
        outer_edge = parse_value(outer_edge, None, defaults, 'outer_edge', ctor=Quantity)

        return aspect_ratio, aspect_flaring, inner_edge, outer_edge

    @classmethod
    def _parse_composition(cls, sigma0, sigma_slope, dust_ratio, defaults=None):
        # Composition properties
        sigma0 = parse_value(sigma0, cls.DEFAULT_SIGMA0, defaults, 'sigma0', ctor=Quantity)
        sigma_slope = parse_value(sigma_slope, cls.DEFAULT_SIGMA_SLOPE, defaults, 'sigma_slope', ctor=Quantity)
        dust_ratio = parse_value(dust_ratio, cls.DEFAULT_DUST_RATIO, defaults, 'dust_ratio', ctor=Quantity)

        return sigma0, sigma_slope, dust_ratio

    @classmethod
    def _parse_kinematic(cls, alpha, defaults=None):
        # Kinematic properties
        alpha = parse_value(alpha, cls.DEFAULT_ALPHA, defaults, 'alpha', ctor=Quantity)

        return alpha

    @classmethod
    def _parse_planet(cls, planet_mass, planet_sma, defaults=None):
        # Planet properties
        planet_mass = parse_value(planet_mass, cls.DEFAULT_PLANET_MASS, defaults, 'planet_mass', ctor=Quantity)
        planet_sma = parse_value(planet_sma, cls.DEFAULT_PLANET_SMA, defaults, 'planet_sma', ctor=Quantity)

        return planet_mass, planet_sma

    @classmethod
    def _parse_ranges(cls, range_mstar_min, range_mstar_max, range_sma_min, range_sma_max, defaults=None):
        range_mstar_min = parse_value(range_mstar_min, cls.DEFAULT_RANGE_MSTAR_MIN, defaults, 'range_mstar_min', ctor=Quantity)
        range_mstar_max = parse_value(range_mstar_max, cls.DEFAULT_RANGE_MSTAR_MAX, defaults, 'range_mstar_max', ctor=Quantity)
        range_sma_min = parse_value(range_sma_min, cls.DEFAULT_RANGE_SMA_MIN, defaults, 'range_sma_min', ctor=Quantity)
        range_sma_max = parse_value(range_sma_max, cls.DEFAULT_RANGE_SMA_MAX, defaults, 'range_sma_max', ctor=Quantity)

        return (range_mstar_min, range_mstar_max, range_sma_min, range_sma_max)

    # Validation
    # =========================================================================
    def _validate_units(cls, units, mstar, planet_sma):
        # Ensure scale mass and radius are compatible with chosen unit system
        if units == DiscUnits.SCALE_FREE:
            if mstar != 1.0:
                raise ValueError(f'mstar={mstar} for {units.name}, expected 1.0')
            if planet_sma != 1.0:
                raise ValueError(f'planet_sma={planet_sma} for {units.name}, expected 1.0')
        else:
            if mstar == 1.0:
                raise ValueError(f'Unexpected mstar={mstar} for {units.name}')
            if planet_sma == 1.0:
                raise ValueError(f'Unexpected planet_sma={planet_sma} for {units.name}')

    # Descriptive properties
    # =========================================================================
    @property
    def name(self):
        return self._name

    @property
    def desc(self):
        return self._desc

    # Unit properties
    # =========================================================================
    @property
    def units(self):
        return self._units

    @property
    def is_scale_free(self):
        return self._units == DiscUnits.SCALE_FREE

    @property
    def mstar(self):
        return self._mstar

    @property
    def consts_G(self):
        if self._units == DiscUnits.SCALE_FREE:
            return 1.0
        elif self._units == DiscUnits.MKS:
            return apc.G.value
        elif self._units == DiscUnits.CGS:
            return apc.G.cgs.value
        else:
            raise NotImplementedError(f'Cannot compute G for {self._units}')

    # Geometric properties
    # =========================================================================
    @property
    def aspect_ratio(self):
        return self._aspect_ratio

    @property
    def aspect_flaring(self):
        return self._aspect_flaring

    @property
    def inner_edge(self):
        return self._inner_edge

    @property
    def outer_edge(self):
        return self._outer_edge

    # Compositional properties
    # =========================================================================
    @property
    def sigma0(self):
        return self._sigma0

    @property
    def sigma_slope(self):
        return self._sigma_slope

    @property
    def dust_ratio(self):
        return self._dust_ratio

    # Kinematic properties
    # =========================================================================
    @property
    def alpha(self):
        return self._alpha

    # Planet properties
    # =========================================================================
    @property
    def planet_mass(self):
        return self._planet_mass

    @property
    def planet_sma(self):
        return self._planet_sma

    # Permitted range properties
    # =========================================================================
    @property
    def range_mstar_min(self):
        return self._range_mstar_min

    @property
    def range_mstar_max(self):
        return self._range_mstar_max

    @property
    def range_sma_min(self):
        return self._range_sma_min

    @property
    def range_sma_max(self):
        return self._range_sma_max

    # Formatting methods
    # =========================================================================
    def __str__(self):
        if self.units == DiscUnits.SCALE_FREE:
            ms = ps = ls = ss = ''
        elif self.units == DiscUnits.CGS:
            ms = apu.M_sun
            ps = apu.M_jup
            ls = apu.AU
            ss = Unit('g/cm2')
        elif self.units == DiscUnits.MKS:
            ms = apu.M_sun
            ps = apu.M_jup
            ls = apu.AU
            ss = Unit('kg/m2')

        q_ratio = (self.planet_mass / self.mstar).to('')

        return textwrap.dedent(f'''\
            NAME:   {self.name}
            DESC:   {self.desc}
            UNITS:  {self.units.name}, M0={self.mstar.to(ms)}, R0={self.planet_sma.to(ls)}
            ASPECT: ratio={self.aspect_ratio}, flare={self.aspect_flaring}
            EDGES:  inner={self.inner_edge.to(ls)}, outer={self.outer_edge.to(ls)}
            SIGMA:  sigma0={self.sigma0.to(ss)}, slope={self.sigma_slope}, dust_ratio={self.dust_ratio}
            VISC:   alpha={self.alpha}
            PLANET: mass={self.planet_mass.to(ps)}, q={q_ratio}, sma={self.planet_sma.to(ls)}
            RANGES: mstar_min={self.range_mstar_min} mstar_max={self.range_mstar_max}
            RANGES: sma_min={self.range_sma_min} sma_max={self.range_sma_max}''')

    def __repr__(self):
        return (f'{self.__class__.__name__}('
            f'name={self.name!r}, '
            f'desc={self.desc!r}, '
            f'units={self.units.name!r}, '
            f'mstar={self.mstar!r}, '
            f'aspect_ratio={self.aspect_ratio!r}, '
            f'aspect_flaring={self.aspect_flaring!r}, '
            f'inner_edge={self.inner_edge!r}, '
            f'outer_edge={self.outer_edge!r}, '
            f'sigma0={self.sigma0!r}, '
            f'sigma_slope={self.sigma_slope!r}, '
            f'dust_ratio={self.dust_ratio!r}, '
            f'alpha={self.alpha!r}, '
            f'planet_mass={self.planet_mass.value!r}, '
            f'planet_sma={self.planet_sma.value!r}, '
            f'range_mstar_min={self.range_mstar_min!r}, '
            f'range_mstar_min={self.range_mstar_max!r}, '
            f'range_sma_min={self.range_sma_min!r}, '
            f'range_sma_min={self.range_sma_max!r})')

    # Serialisation methods
    # =========================================================================
    def to_json(self):
        fmt_qty = lambda q: f'{q.value:g} {q.unit.to_string()}'.strip()

        return json.dumps({
            'version': 1,
            'name': self.name,
            'desc': self.desc,
            'units': self.units.name,
            'mstar': fmt_qty(self.mstar),
            'aspect_ratio': self.aspect_ratio,
            'aspect_flaring': self.aspect_flaring,
            'inner_edge': fmt_qty(self.inner_edge),
            'outer_edge': fmt_qty(self.outer_edge),
            'sigma0': fmt_qty(self.sigma0),
            'sigma_slope': self.sigma_slope,
            'dust_ratio': self.dust_ratio,
            'alpha': self.alpha,
            'planet_mass': fmt_qty(self.planet_mass),
            'planet_sma': fmt_qty(self.planet_sma),
            'range_mstar_min': fmt_qty(self.range_mstar_min),
            'range_mstar_max': fmt_qty(self.range_mstar_max),
            'range_sma_min': fmt_qty(self.range_sma_min),
            'range_sma_max': fmt_qty(self.range_sma_max),
        })

    @classmethod
    def from_json(cls, value):
        kwrds = json.loads(value)
        version = kwrds.pop('version')
        return cls(**kwrds)

    # Computed properties
    # =========================================================================
    def omega_k(self, radius):
        omega_k = (self.consts_G * self.mstar / (radius**3)) ** 0.5
        return omega_k

    def height(self, radius):
        aspect_flare = (radius / self.planet_sma) ** self.aspect_flaring
        height = self.aspect_ratio * aspect_flare * radius
        return height

    def sound_speed(self, radius):
        # ISOTHERMAL!
        sound_speed = self.omega_k(radius) * self.height(radius)
        return sound_speed

    def cpsh_for_cells(self, cells_radial, cells_azimuthal, radius = 1.0):
        step_radial = (self.outer_edge / self.inner_edge).value ** (1 / cells_radial) - 1
        step_azimuthal = 2 * np.pi / cells_azimuthal

        height_at_radius = self.height(radius).value
        cpsh_radial = height_at_radius / (radius * step_radial)
        cpsh_azimuthal = height_at_radius / (radius * step_azimuthal)

        return cpsh_radial, cpsh_azimuthal

    def cells_for_cpsh(self, cpsh_radial, cpsh_azimuthal, radius = 1.0):
        """Return the number of grid cells in the radial and azimuthal axes for
        the given CPSH in the radial and azimuthal axes calculated at the given
        radius. Assumes log spacing in the radial axis."""
        height_at_radius = self.height(radius).value
        step_radial = height_at_radius / (radius * cpsh_radial)
        step_azimuthal = height_at_radius / (radius * cpsh_azimuthal)

        cells_radial = np.log((self.outer_edge / self.inner_edge).value) / np.log(step_radial + 1)
        cells_azimuthal = 2 * np.pi / step_azimuthal

        return np.ceil(cells_radial).astype(np.int32), np.ceil(cells_azimuthal).astype(np.int32)

    # Rescale
    # =========================================================================
    def rescale_disc(self, units, mstar=1.0, planet_sma=1.0):
        # Ensure scale mass and radius are compatible with chosen unit system
        units = DiscUnits.from_any(units)
        self._validate_units(units, mstar, planet_sma)

        # Compute scaling factors
        mass_scale = mstar / self.mstar
        length_scale = planet_sma / self.planet_sma

        # Return a copy of the disc that has been rescaled
        return Disc(
            units = units,
            mstar = self.mstar * mass_scale,
            inner_edge = self.inner_edge * length_scale,
            outer_edge = self.outer_edge * length_scale,
            sigma0 = self.sigma0 * mass_scale / length_scale**2,
            planet_mass = self.planet_mass * mass_scale,
            planet_sma = self.planet_sma * length_scale,
            defaults = self)

