# -*- coding: utf-8 -*-

import glob
import json
import os
import re
import textwrap

import numpy as np

import astropy.constants as apc
from astropy.units import Quantity, Unit

from . import optool
from .utils import parse_value
from . import VILYA_DEFNS_PATH

class DustDatabase:
    # Construction
    # =========================================================================
    def __init__(self, path=os.path.join(VILYA_DEFNS_PATH, 'dust')):
        self._path = path

    def save(self, dust, overwrite=False):
        filename = self._make_filename(dust.name)
        if os.path.exists(filename) and not overwrite:
            raise FileExistsError(filename)
        json_string = dust.to_json()
        with open(filename, 'w') as file:
            file.write(json_string + '\n')

    def load(self, name):
        filename = self._make_filename(name)
        with open(filename) as file:
            json_string = ''.join(file.readlines())
        return Dust.from_json(json_string)

    def delete(self, name):
        filename = self._make_filename(name)
        os.remove(filename)

    def list(self):
        result = []
        for filename in glob.glob(self._make_filename('*', validate=False)):
            dust_name = os.path.splitext(os.path.basename(filename))[0]
            result.append(dust_name)
        return result

    # Validation
    # =========================================================================
    @classmethod
    def validate_name(cls, name):
        # Needs to be something that is valid in the filing system
        if not re.match('^[0-9A-Za-z_:=,@+-]+$', name):
            raise ValueError(f'Dust name {name} contains an invalid character')
        
    # Helper functions
    # =========================================================================
    def _make_filename(self, dust_name, validate=True):
        if validate:
            self.validate_name(dust_name)
        return os.path.join(self._path, dust_name + '.json')

class Dust:
    DEFAULT_MIN_SIZE = Quantity("0.1um")
    DEFAULT_MAX_SIZE = Quantity("1mm")
    DEFAULT_POWER_INDEX = 3.5

    DEFAULT_MATERIAL = 'diana'
    DEFAULT_POROSITY = 0.0

    # Construction
    # =========================================================================
    def __init__(self,
                 name = None,
                 desc = None,
                 min_size = None,
                 max_size = None,
                 power_index = None,
                 material = None,
                 porosity = None,
                 grain_rho = None,
                 defaults = None):

        # Descriptive properties
        name, desc = self._parse_descriptive(
            name = name,
            desc = desc,
            defaults = defaults)

        self._name = name
        self._desc = desc

        # Size properties
        min_size, max_size, power_index = self._parse_size(
            min_size = min_size,
            max_size = max_size,
            power_index = power_index,
            defaults = defaults)

        self._min_size = min_size.to('mm')
        self._max_size = max_size.to('mm')
        self._power_index = power_index.to('').value

        # Material properties
        material, porosity, grain_rho = self._parse_material(
            material = material,
            porosity = porosity,
            grain_rho = grain_rho,
            defaults = defaults)

        grain_rho = grain_rho.to('g/cm3')

        self._material = material
        self._porosity = porosity
        self._grain_rho = grain_rho

    @classmethod
    def _parse_descriptive(cls, name, desc, defaults=None):
        name = parse_value(name, None, defaults, 'name', ctor=str)
        desc = parse_value(desc, None, defaults, 'desc', ctor=str)

        DustDatabase.validate_name(name)

        return name, desc

    @classmethod
    def _parse_size(cls, min_size, max_size, power_index, defaults=None):
        min_size = parse_value(min_size, cls.DEFAULT_MIN_SIZE, defaults, 'min_size', ctor=Quantity)
        max_size = parse_value(max_size, cls.DEFAULT_MAX_SIZE, defaults, 'max_size', ctor=Quantity)
        power_index = parse_value(power_index, cls.DEFAULT_POWER_INDEX, defaults, 'power_index', ctor=Quantity)

        return min_size, max_size, power_index

    @classmethod
    def _parse_material(cls, material, porosity, grain_rho, defaults=None):
        material = parse_value(material, cls.DEFAULT_MATERIAL, defaults, 'material', ctor=str).lower()
        porosity = parse_value(porosity, cls.DEFAULT_POROSITY, defaults, 'porosity', ctor=float)

        material, porosity = optool.lookup_material(material, porosity)
        default_rho = optool.compute_grain_rho(material, porosity)
        grain_rho = parse_value(grain_rho, default_rho, defaults, 'grain_rho', ctor=Quantity)

        return material, porosity, grain_rho

    # Descriptive properties
    # =========================================================================
    @property
    def name(self):
        return self._name

    @property
    def desc(self):
        return self._desc

    # Size properties
    # =========================================================================
    @property
    def min_size(self):
        return self._min_size

    @property
    def max_size(self):
        return self._max_size

    @property
    def power_index(self):
        return self._power_index

    # Material properties
    # =========================================================================
    @property
    def material(self):
        return self._material

    @property
    def porosity(self):
        return self._porosity

    @property
    def grain_rho(self):
        return self._grain_rho

    # Formatting methods
    # =========================================================================
    def __str__(self):
        return textwrap.dedent(f'''\
            NAME:   {self.name}
            DESC:   {self.desc}
            SIZE:   min={self.min_size}, max={self.max_size}, power_index={self.power_index}
            CORE:   {self.material}, porosity={self.porosity}
            RHO:    grain={self.grain_rho}''')

    def __repr__(self):
        return (f'{self.__class__.__name__}('
            f'name={self.name!r}, '
            f'desc={self.desc!r}, '
            f'min_size={self.min_size!r}, '
            f'max_size={self.max_size!r}, '
            f'power_index={self.power_index!r}, '
            f'material={self.material}, '
            f'porosity={self.porosity}, '
            f'grain_rho={self.grain_rho})')

    # Serialisation methods
    # =========================================================================
    def to_json(self):
        fmt_qty = lambda q: f'{q.value:g} {q.unit.to_string()}'.strip()

        return json.dumps({
            'version': 1,
            'name': self.name,
            'desc': self.desc,
            'min_size': fmt_qty(self.min_size),
            'max_size': fmt_qty(self.max_size),
            'power_index': self.power_index,
            'material': self.material,
            'porosity': self.porosity,
            'grain_rho': fmt_qty(self.grain_rho),
        })

    @classmethod
    def from_json(cls, value):
        kwrds = json.loads(value)
        version = kwrds.pop('version')
        return cls(**kwrds)
