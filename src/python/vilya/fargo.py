# -*- coding: utf-8 -*-

from collections import namedtuple
import glob
import os
import re
import subprocess
import sys

import numpy as np

import astropy.constants as apc
import astropy.units as apu
from astropy.units import Quantity

from .disc import Disc, DiscUnits
from .dust import Dust, DustDatabase
from .hydro import Hydro
from . import VILYA_RESULTS_PATH

class FargoConfig:
    DEFAULT_FARGO_SETUP = 'fargo_vilya'
    DEFAULT_DT_PER_ORBIT = 20

    DEFAULT_MIN_DUST_SIZE = Quantity('0.030 um')
    DEFAULT_MAX_DUST_SIZE = Quantity('3.000 mm')
    DEFAULT_MIN_GRAIN_RHO = Quantity('1.00 g/cm3')
    DEFAULT_MAX_GRAIN_RHO = Quantity('3.60 g/cm3')

    MeshSize = namedtuple(
        'MeshSize', [
            'cells_radial',
            'cells_azimuthal',
            'cpsh_radial',
            'cpsh_azimuthal',
            'inner_edge',
            'outer_edge',
        ]
    )

    ScaleRange = namedtuple(
        'ScaleRange', [
            'typ_mstar',
            'min_mstar',
            'max_mstar',
            'typ_r0',
            'min_r0',
            'max_r0',
            'typ_sigma0',
            'min_sigma0',
            'max_sigma0',
            'typ_dust_size',
            'min_dust_size',
            'max_dust_size',
            'typ_grain_rho',
            'min_grain_rho',
            'max_grain_rho',
        ]
    )

    DustBins = namedtuple(
        'DustBins', [
            'ndust',
            'stokes0_numbers',
            'mass_fractions',
            'grain_sizes',
        ]
    )

    def __init__(self, disc, hydro, dust=None):
        # Convert disc into scale free, but keep a copy of the original
        self.orig_disc = disc
        self.disc = disc.rescale_disc(units=DiscUnits.SCALE_FREE)
        self.hydro = hydro
        self.dust = dust

        self.setup = self.DEFAULT_FARGO_SETUP
        self.dt_per_orbit = self.DEFAULT_DT_PER_ORBIT

    def compute_mesh_size(self):
        # Compute mesh size
        mesh_cpsh, cells_radial, cells_azimuthal = self.hydro.mesh_cpsh, self.hydro.mesh_radial, self.hydro.mesh_azimuthal

        cells_cpsh_radial, cells_cpsh_azimuthal = self.disc.cells_for_cpsh(cpsh_radial = mesh_cpsh, cpsh_azimuthal = mesh_cpsh)

        if cells_radial is None: cells_radial = cells_cpsh_radial
        if cells_azimuthal is None: cells_azimuthal = cells_cpsh_azimuthal

        cpsh_radial, cpsh_azimuthal = self.disc.cpsh_for_cells(cells_radial, cells_azimuthal)

        # Apply limits to edges
        inner_edge = max(self.disc.inner_edge, 1.0 * self.hydro.min_edge)
        outer_edge = min(self.disc.outer_edge, 1.0 / self.hydro.min_edge)

        return self.MeshSize(
            cells_radial, cells_azimuthal,
            cpsh_radial, cpsh_azimuthal,
            inner_edge, outer_edge
        )

    def compute_scale_range(self):
        min_mstar = self.orig_disc.range_mstar_min
        max_mstar = self.orig_disc.range_mstar_max
        min_r0 = self.orig_disc.range_sma_min
        max_r0 = self.orig_disc.range_sma_max

        if self.orig_disc.units == DiscUnits.SCALE_FREE:
            typ_mstar = (min_mstar * max_mstar) ** 0.5
            typ_r0 = (min_r0 * max_r0) ** 0.5
            typ_sigma0 = (self.orig_disc.sigma0 * typ_mstar / (typ_r0**2)).to('g/cm2')

            min_sigma0 = min(
                (self.orig_disc.sigma0 * typ_mstar / (max_r0**2)).to('g/cm2'),
                (self.orig_disc.sigma0 * min_mstar / (typ_r0**2)).to('g/cm2')
            )
            max_sigma0 = max(
                (self.orig_disc.sigma0 * typ_mstar / (min_r0**2)).to('g/cm2'),
                (self.orig_disc.sigma0 * max_mstar / (typ_r0**2)).to('g/cm2')
            )
        else:
            # If the original disc was explicitly specified we use precisely those values
            typ_mstar = self.orig_disc.mstar
            typ_r0 = self.orig_disc.planet_sma
            min_sigma0 = max_sigma0 = typ_sigma0 = self.orig_disc.sigma0.to('g/cm2')

        if self.dust is None:
            min_dust_size = self.DEFAULT_MIN_DUST_SIZE
            max_dust_size = self.DEFAULT_MAX_DUST_SIZE
            min_grain_rho = self.DEFAULT_MIN_GRAIN_RHO
            max_grain_rho = self.DEFAULT_MAX_GRAIN_RHO
        else:
            # If the dust was explicitly specifed we used precisely those values
            min_dust_size, max_dust_size = self.dust.min_size, self.dust.max_size
            min_grain_rho = max_grain_rho = self.dust.grain_rho

        typ_dust_size = (min_dust_size * max_dust_size) ** 0.5
        typ_grain_rho = (min_grain_rho * max_grain_rho) ** 0.5

        return self.ScaleRange(
            typ_mstar, min_mstar, max_mstar,
            typ_r0, min_r0, max_r0,
            typ_sigma0, min_sigma0, max_sigma0,
            typ_dust_size, min_dust_size, max_dust_size,
            typ_grain_rho, min_grain_rho, max_grain_rho,
        )

    def compute_dust_bins(self, srng, equal_spacing=True):
        # How many dust bins to compute. Remembers first fluid is always the gas in fargo3d
        ndust = self.hydro.max_fluids - 1

        # Compute the min and max stokes number at r0 given the range of
        min_stokes0 = np.pi/2 * (srng.min_dust_size * srng.min_grain_rho / srng.max_sigma0).to('')
        max_stokes0 = np.pi/2 * (srng.max_dust_size * srng.max_grain_rho / srng.min_sigma0).to('')

        # There are two ways we could divide the bins up:
        # 1) we can make them equal spacing in stokes (size) bins; or
        # 2) we can make them contain equal amount of mass.
        # The first is useful when performing grid searches, whilst the
        # second could be useful for simulating exactly specified systems.
        if equal_spacing:
            stokes0_ratio = (max_stokes0 / min_stokes0) ** (1 / ndust)
            stokes0_numbers = min_stokes0 * stokes0_ratio ** (np.arange(ndust) + 0.5)
        else:
            # We note that Stokes is proportional to dust size
            dust_power = 4 - self.dust.power_index
            cum_fractions = (np.arange(ndust) + 0.5) / ndust
            stokes0_numbers = (
                cum_fractions * max_stokes0**dust_power
                + (1-cum_fractions) * min_stokes0**dust_power) ** (1/dust_power)

        mass_fractions = (self.disc.dust_ratio / ndust) * np.ones(shape=(ndust,))
        grain_sizes = 2 * srng.typ_sigma0 * stokes0_numbers / (np.pi * srng.typ_grain_rho)

        return self.DustBins(
            ndust,
            stokes0_numbers,
            mass_fractions,
            grain_sizes,
        )

    def generate_par_file(self):
        mesh = self.compute_mesh_size()
        srng = self.compute_scale_range()
        dust = self.compute_dust_bins(srng)

        dust_lines = [
            f'UsedFluids              {dust.ndust+1}',
        ]
        for i, (stokes0, mass_fraction, grain_size) in enumerate(zip(dust.stokes0_numbers, dust.mass_fractions, dust.grain_sizes)):
            dust_lines += [
                f'Invstokes{i+1}              {1/stokes0}    typically {grain_size.to("um"):g}',
                f'Epsilon{i+1}                {mass_fraction}',
            ]
        dust_lines = '\n'.join(dust_lines)

        result = f'''\
### Vilya generated fargo3d configuration file
Setup                   {self.setup}
FuncArchFile            func_arch.cfg
VilyaDiscSetup          {self.disc.name}
VilyaDustSetup          {'None' if self.dust is None else self.dust.name}
VilyaHydroSetup         {self.hydro.name}
VilyaTypicalMass        {srng.typ_mstar.to('Msun').value} (in Msun)
VilyaTypicalLength      {srng.typ_r0.to('AU').value} (in AU)
VilyaTypicalGrainRho    {srng.typ_grain_rho.to('g/cm3').value} (in g/cm3)

### Disc geometry parameters

AspectRatio             {self.disc.aspect_ratio}
FlaringIndex            {self.disc.aspect_flaring}
Xmin                    {-np.pi}
Xmax                    {np.pi}
Ymin                    {mesh.inner_edge}      typically {(mesh.inner_edge*srng.typ_r0).to('AU'):g}
Ymax                    {mesh.outer_edge}      typically {(mesh.outer_edge*srng.typ_r0).to('AU'):g}

### Disc gas parameters

Sigma0                  {self.disc.sigma0:e}   typically {srng.typ_sigma0}
SigmaSlope              {self.disc.sigma_slope}

### Disc dust parameters

{dust_lines}
GasFeelsDustDrag        no

### Kinematic parameters

Alpha                   {self.disc.alpha:e}

### Planet parameters

PlanetConfig            dummy-planet.cfg
PlanetMass              {self.disc.planet_mass}
SemiMajorAxis           {self.disc.planet_sma}   typically {srng.typ_r0.to('AU'):g}

Eccentricity            0.0
OrbitalRadius           1.0

ThicknessSmoothing      0.6
RocheSmoothing          0.0

ExcludeHill             no
IndirectTerm            yes

### Mesh parameters

Spacing                 Log
DampingZone             1.15
TauDamp                 0.30

Nx                      {mesh.cells_azimuthal}    CPSH Azi = {mesh.cpsh_azimuthal}
Ny                      {mesh.cells_radial}       CPSH Rad = {mesh.cpsh_radial}

### Frame of reference

OmegaFrame              0.0
Frame                   G

### Output control parameters

DT                      {2*np.pi/self.dt_per_orbit}   Time step length ({self.dt_per_orbit} per orbit)
Ninterm                 {self.hydro.checkpoint*self.dt_per_orbit}    Time steps between outputs ({self.hydro.checkpoint} orbits)
Ntot                    {self.hydro.orbits*self.dt_per_orbit}     Total number of time steps ({self.hydro.orbits} orbits)

OutputDir               @outputs/.
'''

        return result

    def generate_makefile(self, sim_name):
        result = f'''\
SIM_NAME := {sim_name}
SIM_PATH := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))
FINAL_FARGO_CHECKPOINT := {self.hydro.orbits // self.hydro.checkpoint}
FINAL_FARGO_SUMMARY := outputs/summary$(FINAL_FARGO_CHECKPOINT).dat

# We want to be in control of the first (aka default) target of
# the makefile, however we do not know what to build until we
# have read all the separate makefiles in subdirectories.  So
# simply defer to a rule that is defined once all the makefiles
# have been parsed.
.PHONY: all
all: _really_all_

# Gather all the targets from all the component makefiles.
ALL_TARGETS := $(FINAL_FARGO_SUMMARY)
include $(wildcard */Makefile*)

# This is the real rule that makes everything.
.PHONY: _really_all_
_really_all_: $(ALL_TARGETS)

# Run fargo3d to do the hydrodynamic simulation.
$(FINAL_FARGO_SUMMARY):
\t@echo "### Running $(SIM_NAME) hydrodynamic simulation using Fargo3D, this may take some time"
\t@echo "### NB: If interrupted Fargo3D will automatically restart from the latest checkpoint"
\ttime -v ./fargo3d-wrapper $(SIM_NAME).par >>$(SIM_NAME).log 2>&1
'''

        return result

    def make_install(self, path=None, overwrite=False):
        # Apply defaults to output path
        if path is None:
            # Generate a name if none is given
            path = f'{self.disc.name}-{self.hydro.name}'
        if not os.path.isabs(path):
            # Root relative paths wrt the results directory, not cwd
            path = os.path.join(VILYA_RESULTS_PATH, path)
        # Form normalised absolute path
        result_path = os.path.abspath(path)
        # The result name will be the basename (last directory component)
        result_name = os.path.basename(result_path)

        # Generate all data to be saved into files up front
        parameters = self.generate_par_file()
        makefile = self.generate_makefile(result_name)

        # Generate and check all paths and filenames up front
        result_outputs_path = os.path.join(result_path, 'outputs')
        fargo3d_filename = os.path.join(result_path, 'fargo3d')
        fargo3d_wrapper_filename = os.path.join(result_path, 'fargo3d-wrapper')
        par_filename = os.path.join(result_path, result_name + '.par')
        planet_filename = os.path.join(result_path, 'dummy-planet.cfg')
        funcarch_filename = os.path.join(result_path, 'func_arch.cfg')
        makefile_filename = os.path.join(result_path, 'Makefile')

        # Only now actually start doing stuff in the filing system

        # 1) Create directory structure
        os.makedirs(result_path, exist_ok=overwrite)
        os.makedirs(result_outputs_path, exist_ok=overwrite)
        
        # 2) Create fargo executable symbolic link
        if overwrite or not os.path.islink(fargo3d_filename):
            if overwrite and os.path.exists(fargo3d_filename):
                os.remove(fargo3d_filename)
            os.symlink(f'../../vilya-venv/bin/fargo3d-{self.setup}', fargo3d_filename)
        if overwrite or not os.path.islink(fargo3d_wrapper_filename):
            if overwrite and os.path.exists(fargo3d_wrapper_filename):
                os.remove(fargo3d_wrapper_filename)
            os.symlink(f'../../vilya-venv/bin/fargo3d-wrapper', fargo3d_wrapper_filename)

        # 3) Write par file
        if overwrite or not os.path.isfile(par_filename):
            with open(par_filename, 'w') as par_file:
                par_file.write(parameters)

        # 4) Write placeholder planet configuration file
        if overwrite or not os.path.isfile(planet_filename):
            with open(planet_filename, 'w') as planet_file:
                planet_file.write('# This is a dummy placeholder planet configuration file.\n')
                planet_file.write('# The real distance and mass parameters are overridden in the par file.\n')
                planet_file.write('\n')
                planet_file.write('# Planet Name  Distance  Mass  Accretion  Feels Disk  Feels Others\n')
                planet_file.write('Vilya          1.0       0.0   0.0        NO          NO\n')

        # 5) Write func_arch.cfg
        if overwrite or not os.path.isfile(funcarch_filename):
            with open(funcarch_filename, 'w') as funcarch_file:
                funcarch_file.write('# This is a dummy file needed to run fargo3d.\n')
                funcarch_file.write('# This needs to be replaced by a real file if using GPU.\n')

        # 6) Write Makefile
        if overwrite or not os.path.isfile(makefile_filename):
            with open(makefile_filename, 'w') as makefile_file:
                makefile_file.write(makefile)


class FargoFrame(object):
    Dust = namedtuple('Fluid', ['name', 'stokes0', 'mass_ratio', 'size', 'min_size', 'max_size'])

    def __init__(self, path='./outputs', dust=None):
        self.path = path
        
        self._load_vars()
        self._load_parameters()

        if dust is not None:
            if isinstance(dust, Dust):
                self.dust = dust
            else:
                dust_db = DustDatabase()
                self.dust = dust_db.load(dust)
        else:
            try:
                dust_db = DustDatabase()
                self.dust = dust_db.load(self.dust_name)
            except FileNotFoundError:
                self.dust = Dust('default')

        self._load_fluids()
        self._load_grid()

        self.rescale_mass = Quantity(1)
        self.rescale_sma = Quantity(1)
        self.rescale_sigma0 = Quantity(self.sigma0)
        self.unit_time = Quantity(1.0)
        self.load_index()

    def _load_vars(self):
        self.vars = { }
        with open(os.path.join(self.path, 'variables.par')) as var_file:
            for line in var_file:
                name, value = line.strip().split(maxsplit=1)
                try:
                    value = int(value)
                except ValueError:
                    try:
                        value = float(value)
                    except ValueError:
                        pass
                self.vars[name] = value

    def _load_parameters(self):
        self.sigma0 = Quantity(self.vars['SIGMA0'])             # Surface density
        self.sigma_slope = self.vars['SIGMASLOPE']              # Dimensionless
        self.aspect_ratio = self.vars['ASPECTRATIO']            # Dimensionless
        self.aspect_flare = self.vars['FLARINGINDEX']           # Dimensionless
        self.inner_edge = Quantity(self.vars['YMIN'])           # Length
        self.outer_edge = Quantity(self.vars['YMAX'])           # Length
        self.alpha = self.vars['ALPHA']                         # Dimensionless
        self.planet_mass = Quantity(self.vars['PLANETMASS'])    # Mass
        self.planet_sma = Quantity(self.vars['SEMIMAJORAXIS'])  # Length
        self.mstar = Quantity(1.0)                              # Mass
        self.damping_zone = self.vars['DAMPINGZONE']            # Dimensionless

        self.disc_name = self.vars.get('VILYADISCSETUP', None)
        self.dust_name = self.vars.get('VILYADUSTSETUP', None)
        self.hydro_name = self.vars.get('VILYAHYDROSETUP', None)
        self.typical_mstar = self.vars.get('VILYATYPICALMASS', 1.0) * Quantity('1 Msun')
        self.typical_sma = self.vars.get('VILYATYPICALLENGTH', 31.6) * Quantity('1 AU')
        self.typical_grain_rho = self.vars.get('VILYATYPICALGRAINRHO', 1.90) * Quantity('1 g/cm3')
        self.typical_sigma0 = (self.sigma0 * self.typical_mstar / self.typical_sma**2).to('g/cm2')

    def _load_fluids(self):
        self.gas_name = 'gas'
        if 'USEDFLUIDS' in self.vars:
            self.num_dusts = self.vars['USEDFLUIDS']
        else:
            self.num_dusts = 1
            while f'INVSTOKES{self.num_dusts}' in self.vars:
                self.num_dusts += 1
        self.dust_names = [ f'dust{n}' for n in range(0, self.num_dusts) ]

    def _calc_fluids(self):
        # We will need to handle quantities differently depending on whether
        # we are in scale free setting or not.
        size_unit = '' if self.is_scale_free else 'um'
        density_unit = '' if self.is_scale_free else 'g/cm3'

        self.grain_rho = Quantity(self.dust.grain_rho)
        dust_min_size = Quantity(self.dust.min_size)
        dust_max_size = Quantity(self.dust.max_size)
        gas_min_size = Quantity('0.001um')  # Consider small dust dynamically coupled to gas
        power_index = self.dust.power_index

        if self.is_scale_free:
            self.grain_rho = self.grain_rho / (self.typical_mstar / self.typical_sma**3)
            dust_min_size /= self.typical_sma
            dust_max_size /= self.typical_sma
            gas_min_size /= self.typical_sma

        # Make sure quantities and units are all correct.
        self.grain_rho = self.grain_rho.to(density_unit)
        dust_min_size = dust_min_size.to(size_unit)
        dust_max_size = dust_max_size.to(size_unit)
        gas_min_size = gas_min_size.to(size_unit)

        # Leave space to fill in the "gas" dust later ...
        stokes0s = [ 0.0 ]
        min_sizes = [ dust_min_size ]
        typ_sizes = [ dust_min_size ]
        max_sizes = [ dust_min_size ]
        total_dust_frac = 0.0

        # Process each of the real dust species
        for species in range(1, self.num_dusts):
            spec_stokes0 = 1.0 / self.vars[f'INVSTOKES{species}']
            spec_typ_size = 2 * spec_stokes0 * self.sigma0 / (np.pi * self.grain_rho)

            spec_min_size = max_sizes[-1]
            if spec_min_size is None:
                # Find boundary between both typical sizes
                spec_min_size = (typ_sizes[-1] * spec_typ_size) ** 0.5
                max_sizes[-1] = spec_min_size

            stokes0s.append(spec_stokes0)
            min_sizes.append(spec_min_size)
            typ_sizes.append(spec_typ_size)
            max_sizes.append(None)
            total_dust_frac += self.vars.get(f'EPSILON{species}', self.vars.get('EPSILON', 0.01))

        max_sizes[-1] = max(dust_max_size, min_sizes[-1] * (typ_sizes[-1]/min_sizes[-1])**2)

        # Handle the gas fluid, which is always the first
        # Compute the size of dust that is well coupled to the gas, we
        # define this to be the maximum size where 50% of the density
        # is still present at 3 scale heights.  This leads to roughly
        # 99% overlap between the gas and dust distributions (i.e.
        # very highly coupled).  NB This is at R0, and the size
        # does vary inversely by radius.
        frac = 0.50
        num_stdev = 3
        scaling_factor = -np.log(frac) / (np.exp(0.5 * num_stdev**2) - 1)
        gas_stokes0 = min(scaling_factor * self.alpha, stokes0s[1])
        gas_max_size = min(2 * gas_stokes0 * self.sigma0 / (np.pi * self.grain_rho), typ_sizes[1])
        gas_min_size = min(gas_min_size, gas_max_size)
        gas_typ_size = (gas_min_size * gas_max_size) ** 0.5
        gas_stokes0 = (np.pi / 2 * gas_typ_size * self.grain_rho / self.sigma0).to('').value

        stokes0s[0] = gas_stokes0
        min_sizes[0] = gas_min_size
        max_sizes[0] = min_sizes[1] = gas_max_size
        typ_sizes[0] = gas_typ_size

        # Now compute the mass fraction for each dust species, note that
        # this can be zero in the case that the size_bin is outside of
        # the range of the dust size itself.
        self.dusts = []
        for species in range(0, self.num_dusts):
            spec_stokes0 = stokes0s[species]
            spec_min_size = min_sizes[species]
            spec_typ_size = typ_sizes[species]
            spec_max_size = max_sizes[species]

            mass_ratio = (total_dust_frac
                * ((np.clip(spec_max_size, gas_min_size, dust_max_size)**(4-power_index)
                  - np.clip(spec_min_size, gas_min_size, dust_max_size)**(4-power_index)))
                / (dust_max_size**(4-power_index) - gas_min_size**(4-power_index))).to('')

            self.dusts.append(
                self.Dust(self.dust_names[species], spec_stokes0, mass_ratio,
                          spec_typ_size.to(size_unit), spec_min_size.to(size_unit), spec_max_size.to(size_unit))
            )

    def _load_grid(self):
        self.phi_edge = Quantity(np.fromfile(os.path.join(self.path, 'domain_x.dat'), sep='\n'))
        self.r_edge = Quantity(np.fromfile(os.path.join(self.path, 'domain_y.dat'), sep='\n')[3:-3])

        self.phi_centre = 0.5 * (self.phi_edge[:-1] + self.phi_edge[1:])       # Arithmetic mean
        self.r_centre = (self.r_edge[:-1] * self.r_edge[1:]) ** 0.5            # Geometric mean

        self.num_phi = len(self.phi_centre)
        self.num_r = len(self.r_centre)

        # Sanity check
        if self.vars['NX'] != self.num_phi:
            raise ValueError('Mismatching number of azimuthal cells')
        if self.vars['NY'] != self.num_r:
            raise ValueError('Mismatching number of radial cells')
        if self.vars['NZ'] != 1:
            raise ValueError('Mismatching number of vertical cells')

    def _calc_heights(self):
        self.aspect_centre = self.aspect_ratio * (self.r_centre / self.planet_sma) ** self.aspect_flare
        self.height_centre = self.r_centre * self.aspect_centre

    def _calc_orbit(self):
        G = (1.0 if self.is_scale_free else apc.G)
        time_unit = ('' if self.is_scale_free else 'Myr')

        sma = self.planet_sma
        mass = self.mstar

        self.orbit = self.index * self.vars['NINTERM'] * self.vars['DT'] / (2 * np.pi)
        self.time = (self.orbit * 2 * np.pi * self.unit_time).to(time_unit)

    @property
    def is_scale_free(self):
        return self.mstar.unit.is_unity()

    @property
    def final_index(self):
        index = 0
        pattern = re.compile('gasdens([0-9]+)\.dat$')
        for filename in glob.glob(os.path.join(self.path, 'gasdens[0-9]*.dat')):
            match = pattern.search(filename)
            if match:
                match_index = int(match.group(1))
                index = max(index, match_index)

        return index

    def load_index(self, index=None):
        if index is None:
            index = self.final_index
        self.index = index

        self._calc_heights()
        self._calc_fluids()
        self._calc_orbit()

        sigma_ratio = self.mstar / self.planet_sma**2
        sigma_units = '' if self.is_scale_free else 'g/cm2'

        self.sigma = { }
        for species, dust in enumerate(self.dusts):
            if dust.name == 'dust0':
                computed_frac = 1.0
                raw_name = self.gas_name
            else:
                computed_frac = self.vars.get(f'EPSILON{species}', self.vars.get('EPSILON', 0.01))
                raw_name = dust.name

            required_frac = dust.mass_ratio
            dust_ratio = required_frac / computed_frac

            try:
                filename = os.path.join(self.path, f'{raw_name}dens{index}.dat')
                data = np.fromfile(filename).reshape(self.num_r, self.num_phi)
                self.sigma[dust.name] = Quantity(data * sigma_ratio * dust_ratio).to(sigma_units)

                if raw_name != dust.name:
                    self.sigma[raw_name] = Quantity(data * sigma_ratio).to(sigma_units)
            except FileNotFoundError:
                pass

        cs_units = '' if self.is_scale_free else 'cm/s'
        cs_ratio = self.planet_sma / self.unit_time

        try:
            filename = os.path.join(self.path, f'{self.gas_name}energy{index}.dat')
            data = np.fromfile(filename).reshape(self.num_r, self.num_phi)
            self.cs_gas = Quantity(data * cs_ratio).to(cs_units)
        except FileNotFoundError:
            pass

    def set_scale_free(self):
        self.rescale_mass = Quantity(1)
        self.rescale_sma = Quantity(1)
        self.rescale_sigma0 = self.rescale_sigma0 * self.planet_sma**2 / self.mstar
        self._rescale()

    def set_scale(self, mstar=None, planet_sma=None, sigma0=None):
        if mstar is None:
            mstar = self.typical_mstar
        else:
            mstar = Quantity(mstar)
        self.rescale_mass = mstar

        if planet_sma is None:
            planet_sma = self.typical_sma
        else:
            planet_sma = Quantity(planet_sma)
        self.rescale_sma = planet_sma

        if sigma0 is None:
            mass_ratio = self.rescale_mass / self.mstar
            length_ratio = self.rescale_sma / self.planet_sma
            sigma0 = self.rescale_sigma0 * mass_ratio / length_ratio**2
        self.rescale_sigma0 = Quantity(sigma0)

        self._rescale()

    def _rescale(self):
        mass_ratio = self.rescale_mass / self.mstar
        length_ratio = self.rescale_sma / self.planet_sma

        self.mstar *= mass_ratio
        self.planet_mass *= mass_ratio

        self.planet_sma *= length_ratio
        self.inner_edge *= length_ratio
        self.outer_edge *= length_ratio
        self.r_edge = self.r_edge * length_ratio
        self.r_centre = self.r_centre * length_ratio

        sigma_units = '' if self.is_scale_free else 'g/cm2'
        self.rescale_sigma0 = self.rescale_sigma0.to(sigma_units)
        self.sigma0 = self.rescale_sigma0

        if self.is_scale_free:
            self.unit_time = Quantity(1.0)
        else:
            self.unit_time = ((self.planet_sma**3 / (apc.G * self.mstar)) ** 0.5).to('s')

        # Force recomputation of dust sizes etc
        self.load_index(self.index)

    def dump(self):
        cell_area = (self.phi_edge[None,1:] - self.phi_edge[None,:-1]) * self.r_centre[:,None] * (self.r_edge[1:,None] - self.r_edge[:-1,None])

        print(f'PATH:   {self.path}')
        print(f'SETUP:  disc={self.disc_name}, hydro={self.hydro_name}, dust={self.dust.name}')
        print(f'MASS:   mstar={self.mstar:g}, q={self.planet_mass/self.mstar:g} (planet={self.planet_mass:g})')
        print(f'SIGMA:  sigma0={self.sigma0:g}, slope={self.sigma_slope:g}')
        print(f'ASPECT: ratio={self.aspect_ratio:g}, slope={self.aspect_flare:g}')
        print(f'DISTS:  inner={self.inner_edge:g}, outer={self.outer_edge:g}, planet_sma={self.planet_sma:g}')
        print(f'VISC:   alpha={self.alpha:g}')
        print(f'MESH:   num_radial={self.num_r}, num_phi={self.num_phi}')
        print(f'TIME:   index={self.index}, orbit={self.orbit:g}, time={self.time:g}')
        total_mass = np.sum(self.sigma[self.gas_name] * cell_area).to(self.mstar.unit)
        print(f'GAS:    total_mass={total_mass:g}')
        for dust in self.dusts:
            total_mass = np.sum(self.sigma[dust.name] * cell_area).to(self.mstar.unit)
            print(f'{dust.name}:  stokes0={dust.stokes0:g}, typical_size={dust.size:g}, size_bin=({dust.min_size:g}, {dust.max_size:g}), mass_ratio={dust.mass_ratio:g}, total_mass={total_mass:g}')
        print()


def restartable_wrapper():
    # Find all generated summary files
    pattern = re.compile('summary([0-9]+).dat$')

    # Find the latest (i.e. largest) checkpoint
    max_checkpoint = -1
    for filename in glob.glob('outputs/summary*.dat'):
        matched = pattern.search(filename)
        if matched:
            max_checkpoint = max(max_checkpoint, int(matched.group(1)))

    # Form the command line to execute
    command = ['mpirun', '-np', str(os.cpu_count() // 2), './fargo3d']
    if max_checkpoint > 0:
        command += ['-S', str(max_checkpoint)]
    command += sys.argv[1:]

    print ('Executing:', command)
    result = subprocess.run(args=command)
    print (result)
    sys.exit(result.returncode)
