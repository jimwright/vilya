#!/usr/bin/env bash

# ==============================================================================
# CLEAR OUT ALL EXISTING DEFINITIONS
# ==============================================================================
rm defns/*/*.json

# ==============================================================================
# DUST SETUPS
# ==============================================================================
vilya dust define diana_i35 --material diana --power-index 3.5 --min-size 0.01um --max-size 0.1mm
vilya dust define diana_i25 --material diana --power-index 2.5 --min-size 0.01um --max-size 10.0mm
vilya dust define dsharp_i35 --material dsharp --power-index 3.5 --min-size 0.01um --max-size 0.1mm
vilya dust define dsharp_i25 --material dsharp --power-index 2.5 --min-size 0.01um --max-size 10.0mm

# ==============================================================================
# HYDRO SETUPS
# ==============================================================================
vilya hydro define tiny --preset tiny
vilya hydro define small --preset small
vilya hydro define medium --preset medium
vilya hydro define large --preset large
vilya hydro define huge --preset huge

# ==============================================================================
# DISC SETUPS
# ==============================================================================
vilya disc define default

# ==============================================================================
# SYSTEM SETUPS
# ==============================================================================
vilya system define default --distance 140pc --mstar 1.0Msun --rstar 2.8Rsun --tstar 4265K --planet-sma 31.62AU
