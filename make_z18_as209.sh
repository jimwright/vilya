#!/usr/bin/env bash

# Create the vilya definitions for the models of AS209 used in Zhang et al 2018

# ==============================================================================
# CLEAR OUT OLD DEFINITIONS
# ==============================================================================
rm -fv defns/*/z18_as209*.json

# Material: Zhang 2018, Page 4
# Min Size: Zhang 2018, Page 3  (when combined with vilya's gas as dust feature)
# Max Size: Zhang 2018, Fig 19(b)
# Power Index: Zhang 2018, Page 19
vilya dust define z18_as209 \
        --material dsharp \
        --min-size 10um \
        --max-size 0.3mm \
        --power-index 3.5

# Stellar Mass: Zhang 2018, Table 3
# Aspect: Zhang 2018, Page 19
# Edges: Zhang 2018, Page 3 - but here we move the inner edge for performance reasons
# Sigma: Zhang 2018, Fig 19(b)
# DustRatio: Assumed to be 0.01, but can be scaled post simulation freely
# Alpha: Zhang 2018, Page 19, Fig 19(b)
# PlanetSMA: Zhang 2018, Table 3
# PlanetMass: Zhang 2018, Page 19
vilya disc define z18_as209 \
        --units cgs \
        --mstar 0.83Msun \
        --aspect 0.05,0.25 \
        --edges 0.10,4.00 \
        --sigma 15g/cm2,1.0 \
        --dust-ratio 0.01 \
        --alpha 1e-5 \
        --planet-sma 99AU \
        --planet-mass 1e-4

# Orbits: Zhang 2018, Fig 19(b)
# CPSH: Zhang 2018, Page 3 - but here we reduce for performance reasons
# MaxFluids: Different code and method used (Fargo3d vs Dusty Fargo-ADSG)
vilya hydro define z18_as209 \
        --preset large \
        --orbits 2000 \
        --checkpoint 25 \
        --mesh-cpsh 10 \
        --max-fluids 4 \
        --min-edge 0.15

# Distance: Andrews 2018, Table 1
# Coord: Fiducial Fits File for AS209 DSHARP Data Release
# Incl: Huang 2018, Table 1
# PosAng: Huang 2018, Table 1
# MStar: Andrews 2018, Table 1
# RStar: Andrews 2018, Table 1
# TStar: Andrews 2018, Table 1
# PlanetSMA: Zhang 2018, Table 3
vilya system define z18_as209 \
	--distance 121pc \
	--coord "16h49m15.29s -14d22m9.05s" \
	--incl 35.0 \
	--posang 85.7 \
	--mstar 0.83Msun \
	--rstar 2.18Rsun \
	--tstar 4265K \
	--planet-sma 99AU

vilya simulate z18_as209 --disc z18_as209 --dust z18_as209 --hydro z18_as209

vilya radiate z18_as209/as209 --system z18_as209

vilya observe z18_as209/as209/alma10.6 --system z18_as209 --config alma10.6
vilya observe z18_as209/as209/alma10.7 --system z18_as209 --config alma10.7
vilya observe z18_as209/as209/alma10.8 --system z18_as209 --config alma10.8
vilya observe z18_as209/as209/dsharp1 --system z18_as209 --config dsharp1
vilya observe z18_as209/as209/dsharp24 --system z18_as209 --config dsharp24
vilya observe z18_as209/as209/dsharp96 --system z18_as209 --config dsharp96
vilya observe z18_as209/as209/dsharp384 --system z18_as209 --config dsharp384
