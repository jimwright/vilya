#!/usr/bin/env bash

# Create the vilya definitions for the grid of models used in Zhang et al 2018

# ==============================================================================
# CLEAR OUT OLD DEFINITIONS
# ==============================================================================
rm -fv defns/*/z18g_*.json

# ==============================================================================
# DUST SETUPS
# ==============================================================================
# No explicit material was specified by the paper except with the usual
# average density of 1g/cm3.  This is not realisi
vilya dust define z18g_dsd1 --material dsharp --min-size 10um --max-size 0.1mm --power-index 3.5
vilya dust define z18g_dsd2 --material dsharp --min-size 10um --max-size 1.0cm --power-index 2.5

# ==============================================================================
# HYDRO SETUPS
# ==============================================================================
vilya hydro define z18g_base --preset large --orbits 1000 --checkpoint 100 --max-fluids 4 --min-edge 0.2
vilya hydro define z18g_am2 --copy z18g_base --mesh-explicit 384,256
vilya hydro define z18g_am3 --copy z18g_base --mesh-explicit 512,384
vilya hydro define z18g_am4 --copy z18g_base --mesh-explicit 768,512

# ==============================================================================
# DISC SETUPS
# ==============================================================================
declare -A ASPECTS=(
        [h05]="--aspect 0.05,0.25"
        [h07]="--aspect 0.07,0.25"
        [h10]="--aspect 0.10,0.25"
)
declare -A ALPHAS=(
        [am2]="--alpha 1e-2"
        [am3]="--alpha 1e-3"
        [am4]="--alpha 1e-4"
)
declare -A PLANETS=(
#       [p1]="--planet-mass 3.3e-5"
        [p2]="--planet-mass 1.0e-4"
        [p3]="--planet-mass 3.3e-4"
        [p4]="--planet-mass 1.0e-3"
        [p5]="--planet-mass 3.3e-3"
)

vilya disc define z18g_base --edges 0.2 --sigma 0.0033 --dust-ratio 0.01 --range-mstar 1Msun --range-sma 10AU,100AU
for H in ${!ASPECTS[@]}; do
    for A in ${!ALPHAS[@]}; do
        for Q in ${!PLANETS[@]}; do
            DISC_NAME="z18g_$H$A$Q"
            HYDRO_NAME="z18g_$A"
            vilya disc define $DISC_NAME --copy z18g_base ${ASPECTS[$H]} ${ALPHAS[$A]} ${PLANETS[$Q]}
            vilya simulate $DISC_NAME --disc $DISC_NAME --hydro $HYDRO_NAME --dust z18g_dsd2
        done
    done
done
