# Vilya #

## Introduction ##

Vilya's goal is to provide a self contained environment to generate synthetic images of protoplanetary discs.

## Pipeline ##

The Vilya pipeline:

1. Define disc definitions
1. Define hydrodynamical simulation setups 

## Working with disc definitions ##

1. **Descriptive:** names and descriptions
1. **Units:** scale free, cgs or mks
1. **Geometry:** aspect ratio, flaring and inner/outer edges
1. **Composition:** gas and dust profiles
1. **Kinematics:** viscosity prescription
1. **Planet:** mass and semi-major axis

Vilya provides multiple commands for manipulating disc definitions.  Each command starts with `vilya disc`.

```
vilya disc define
vilya disc info
vilya disc list
vilya disc remove
```

**TODO**

## Working with hydrodynamical codes ##

1. **Code:** fargo3d
1. **


Vilya provides multiple commands for manipulating disc definitions.  Each command starts with `vilya hydro`.

```
vilya hydro define
vilya hyrdo info
vilya hyrdo list
vilya hydro remove
```

## Running a hydrodynamical simulation ##

vilya simulate