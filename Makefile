SYSTEM_PYTHON	:=	python3.10
SYSTEM_GIT	:=	git
SYSTEM_CP	:=	cp
SYSTEM_RM	:=	rm

FARGO3D_CONFIG	:=	PARALLEL=1 GPU=0
FARGO3D_SETUPS	:=	fargo_vilya

# ======================================================================

ROOT_PATH	:=	$(dir $(abspath $(firstword $(MAKEFILE_LIST)))).
SRC_PATH	:=	$(ROOT_PATH)/src
EXT_PATH	:=	$(ROOT_PATH)/external
VENV_PATH	:=	$(ROOT_PATH)/vilya-venv

# ======================================================================

SRC_PYTHON      :=      $(SRC_PATH)/python

EXT_FARGO3D	:=	$(EXT_PATH)/fargo3d
EXT_OPTOOL	:=	$(EXT_PATH)/optool
EXT_RADMC3D	:=	$(EXT_PATH)/radmc3d-2.0

VENV_BIN	:=	$(VENV_PATH)/bin
VENV_ACTIVATE	:=	$(VENV_BIN)/activate
VENV_PIP	:=	$(VENV_BIN)/pip3
VENV_PYTHON	:=	$(VENV_BIN)/python3

# ======================================================================

.PHONY: build clean

build: venv vilya fargo3d optool radmc3d

clean: venv_clean vilya_clean fargo3d_clean optool_clean radmc3d_clean

# ======================================================================

.PHONY: venv venv_clean

venv: $(VENV_ACTIVATE)

venv_clean:
	"$(SYSTEM_RM)" -rf "$(VENV_PATH)"

$(VENV_ACTIVATE):
	"$(SYSTEM_GIT)" submodule update --init --recursive
	"$(SYSTEM_PYTHON)" -m venv "$(VENV_PATH)"
	"$(VENV_PYTHON)" -m ensurepip --upgrade
	"$(VENV_PIP)" --require-virtualenv install 'setuptools'
	"$(VENV_PIP)" --require-virtualenv install 'numpy<1.24'
	"$(VENV_PIP)" --require-virtualenv install 'ipython'
	"$(VENV_PIP)" --require-virtualenv install 'matplotlib'
	"$(VENV_PIP)" --require-virtualenv install 'scipy'
	"$(VENV_PIP)" --require-virtualenv install 'astropy'
	"$(VENV_PIP)" --require-virtualenv install 'tabulate'
	"$(VENV_PIP)" --require-virtualenv install 'dsharp_opac'
	"$(VENV_PIP)" --require-virtualenv install 'casatasks'

# ======================================================================

.PHONY: jupyterlab

jupyterlab: $(VENV_ACTIVATE)
	"$(VENV_PIP)" --require-virtualenv install 'jupyterlab'

# ======================================================================

.PHONY: vilya vilya_clean vilya_install

vilya: $(VENV_BIN)/vilya

vilya_clean:
	"$(SYSTEM_RM)" -fv "$(VENV_BIN)/vilya"

$(VENV_BIN)/vilya: $(VENV_ACTIVATE)
	cd "$(SRC_PYTHON)" && "$(VENV_PYTHON)" setup.py install

vilya_install: $(VENV_ACTIVATE)
	cd "$(SRC_PYTHON)" && "$(VENV_PYTHON)" setup.py install

# ======================================================================

.PHONY: fargo3d fargo3d_clean

fargo3d: $(addprefix $(VENV_BIN)/fargo3d-,$(FARGO3D_SETUPS))

fargo3d_clean:
	"$(MAKE)" -C "$(EXT_FARGO3D)" mrproper
	"$(SYSTEM_RM)" -fv "$(VENV_BIN)/fargo3d-"*

$(VENV_BIN)/fargo3d-%: $(VENV_ACTIVATE) $(EXT_FARGO3D)/setups/%/condinit.c
	"$(MAKE)" -C "$(EXT_FARGO3D)" mrproper
	"$(MAKE)" -C "$(EXT_FARGO3D)" "SETUP=$*" $(FARGO3D_CONFIG)
	"$(SYSTEM_CP)" -fv "$(EXT_FARGO3D)/fargo3d" "$@"

# ======================================================================

.PHONY: optool optool_clean

optool: $(VENV_BIN)/optool

optool_clean:
	"$(MAKE)" -C "$(EXT_OPTOOL)" clean
	"$(SYSTEM_RM)" -fv "$(VENV_BIN)/optool"

$(VENV_BIN)/optool: $(VENV_ACTIVATE)
	"$(MAKE)" -C "$(EXT_OPTOOL)" multi=true
	"$(SYSTEM_CP)" -fv "$(EXT_OPTOOL)/optool" "$@"
	cd "$(EXT_OPTOOL)" && "$(VENV_PYTHON)" setup.py install

# ======================================================================

.PHONY: radmc3d radmc3d_clean

radmc3d: $(VENV_BIN)/radmc3d

radmc3d_clean:
	"$(MAKE)" -C "$(EXT_RADMC3D)/src" clean
	"$(SYSTEM_RM)" -fv "$(VENV_BIN)/radmc3d"

$(VENV_BIN)/radmc3d: $(VENV_ACTIVATE)
	"$(MAKE)" -C "$(EXT_RADMC3D)/src"
	"$(SYSTEM_CP)" -fv "$(EXT_RADMC3D)/src/radmc3d" "$@"
	cd "$(EXT_RADMC3D)/python/radmc3dPy" && "$(VENV_PYTHON)" setup.py build
	cd "$(EXT_RADMC3D)/python/radmc3dPy" && "$(VENV_PYTHON)" setup.py install

# ======================================================================
