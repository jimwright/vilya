#!/usr/bin/env python3

# A simple driver to test radmc3d with different disc confirurations

import argparse
import os
import subprocess
import struct

import numpy as np
from scipy.optimize import bisect

from astropy.units import Quantity, Unit

# Setup our standard unit system
WAVELENGTH_UNIT = Unit('um')
LENGTH_UNIT = Unit('cm')
MASS_UNIT = Unit('g')
TEMP_UNIT = Unit('K')
SIGMA_UNIT = MASS_UNIT / LENGTH_UNIT**2
RHO_UNIT = MASS_UNIT / LENGTH_UNIT**3

DUST_DIR = 'dustopac'

# A simple grid refinement function
def grid_refine_inner_edge(x_orig,nlev,nspan):
    x     = x_orig.copy()
    rev   = x[0]>x[1]
    for ilev in range(nlev):
        x_new = 0.5 * ( x[1:nspan+1] + x[:nspan] )
        x_ref = np.hstack((x,x_new))
        x_ref.sort()
        x     = x_ref
        if rev:
            x = x[::-1]
    return x

# Parse parameters
def parse_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--mstar',
                        default = Quantity('1.5Msun'),
                        type = Quantity,
                        metavar = 'MASS',
                        help = 'Star mass (DEFAULT: %(default)s)')
    parser.add_argument('--rstar',
                        default = Quantity('2.1Rsun'),
                        type = Quantity,
                        metavar = 'RADIUS',
                        help = 'Star radius (DEFAULT: %(default)s)')
    parser.add_argument('--tstar',
                        default = Quantity('4265K'),
                        type = Quantity,
                        metavar = 'TEMP',
                        help = 'Star temperature (DEFAULT: %(default)s)')

    parser.add_argument('--rmin',
                        default = Quantity('10AU'),
                        type = Quantity,
                        metavar = 'DIST',
                        help = 'Inner disc radius (DEFAULT: %(default)s)')
    parser.add_argument('--rmax',
                        default = Quantity('100AU'),
                        type = Quantity,
                        metavar = 'DIST',
                        help = 'Outer disc radius (DEFAULT: %(default)s)')
    parser.add_argument('--r0',
                        default = Quantity('30AU'),
                        type = Quantity,
                        metavar = 'DIST',
                        help = 'Planet orbital radius (DEFAULT: %(default)s)')
    parser.add_argument('--nr',
                        default = 128,
                        type = int,
                        metavar = 'NUMBER',
                        help = 'Number of radial grid cells (DEFAULT: %(default)s)')
    parser.add_argument('--ntheta',
                        default = 32,
                        type = int,
                        metavar = 'NUMBER',
                        help = 'Number of vertical cells (DEFAULT: %(default)s)')
    parser.add_argument('--nphi',
                        default = 1,
                        type = int,
                        metavar = 'NUMBER',
                        help = 'Number of azimuthal grid cells (DEFAULT: %(default)s)')
    parser.add_argument('--refine-r',
                        default = False,
                        action = 'store_true',
                        help = 'Refine the inner edge of the disc (DEFAULT: %(default)s)')
    parser.add_argument('--refine-theta',
                        default = False,
                        action = 'store_true',
                        help = 'Refine the mid plane of the disc (DEFAULT: %(default)s)')

    parser.add_argument('--sigma0',
                        default = Quantity('10 g/cm2'),
                        type = Quantity,
                        metavar = 'SURF-DENSITY',
                        help = 'Gas surface density at r=r0 (DEFAULT: %(default)s)')
    parser.add_argument('--aspect-ratio',
                        default = 0.1,
                        type = float,
                        metavar = 'RATIO',
                        help = 'Aspect ratio at r=r0 (DEFAULT: %(default)s)')
    parser.add_argument('--alpha',
                        default = 0.001,
                        type = float,
                        metavar = 'NUMBER',
                        help = 'Disc viscosity (DEFAULT: %(default)s)')

    parser.add_argument('--ndust',
                        default = 3,
                        type = int,
                        metavar = 'NUMBER',
                        help = 'Number of dust sizes (DEFAULT: %(default)s)')
    parser.add_argument('--dust-ratio',
                        default = 0.01,
                        type = float,
                        metavar = 'RATIO',
                        help = 'Mass ratio of dust to gas (DEFAULT: %(default)s)')
    parser.add_argument('--dust-power-index',
                        default = -3.5,
                        type = float,
                        metavar = 'INDEX',
                        help = 'Power law index for dust *number* density (DEFAULT: %(default)s)')
    parser.add_argument('--grain-material',
                        default = 'diana',
                        type = str,
                        metavar = 'MATERIAL',
                        help = 'Dust grain material (DEFAULT: %(default)s)')
    parser.add_argument('--grain-rho',
                        default = Quantity('2.08 g/cm3'),
                        type = Quantity,
                        metavar = 'DENSITY',
                        help = 'Dust grain density (DEFAULT: %(default)s)')
    parser.add_argument('--grain-minsize',
                        default = Quantity('0.1 um'),
                        type = Quantity,
                        metavar = 'SIZE',
                        help = 'Minimum dust grain size (DEFAULT: %(default)s)')
    parser.add_argument('--grain-maxsize',
                        default = Quantity('1000 um'),
                        type = Quantity,
                        metavar = 'SIZE',
                        help = 'Maximum dust grain size (DEFAULT: %(default)s)')

    parser.add_argument('--nphot-therm',
                        default = 10000000,
                        type = int,
                        metavar = 'NUMBER',
                        help = 'Number of mctherm photos (DEFAULT: %(default)s)')
    parser.add_argument('--nphot-scat',
                        default = 1000000,
                        type = int,
                        metavar = 'NUMBER',
                        help = 'Number of scat photos (DEFAULT: %(default)s)')

    parser.add_argument('name',
                        type = str,
                        help = 'Name of subdirectory to create for radmc')

    args = parser.parse_args()

    # Validate
    if args.nphi != 1:
        raise ValueError('Only nphi=1 currently supported')

    # Apply expected scaling factors
    if args.mstar.unit.is_unity(): args.mstar *= Unit('Msun')
    if args.rstar.unit.is_unity(): args.rstar *= Unit('Rsun')
    if args.tstar.unit.is_unity(): args.tstar *= Unit('K')
    if args.rmin.unit.is_unity(): args.rmin *= Unit('AU')
    if args.rmax.unit.is_unity(): args.rmax *= Unit('AU')
    if args.r0.unit.is_unity(): args.r0 *= Unit('AU')
    if args.sigma0.unit.is_unity(): args.sigma0 *= Unit('g/cm2')
    if args.grain_rho.unit.is_unity(): args.grain_rho *= Unit('g/cm3')
    if args.grain_minsize.unit.is_unity(): args.grain_minsize *= Unit('um')
    if args.grain_maxsize.unit.is_unity(): args.grain_maxsize *= Unit('um')

    # Dump out the parsed values
    print(f'NAME       = {args.name}')
    print(f'MSTAR      = {args.mstar:g}')
    print(f'RSTAR      = {args.rstar:g}')
    print(f'TSTAR      = {args.tstar:g}')
    print(f'RMIN       = {args.rmin:g}')
    print(f'RMAX       = {args.rmax:g}')
    print(f'R0         = {args.r0:g}')
    print(f'SIGMA0     = {args.sigma0:g}')
    print(f'ASPECT     = {args.aspect_ratio}')
    print(f'ALPHA      = {args.alpha}')
    print(f'NDUST      = {args.ndust:g}')
    print(f'DUST-RATIO = {args.dust_ratio}')
    print(f'DUST-PINDEX= {args.dust_power_index}')
    print(f'GRAIN-MAT  = {args.grain_material}')
    print(f'GRAIN-RHO  = {args.grain_rho:g}')
    print(f'GRAIN-MINSZ= {args.grain_minsize:g}')
    print(f'GRAIN-MAXSZ= {args.grain_maxsize:g}')
    print(f'NR         = {args.nr:g}')
    print(f'NTHETA     = {args.ntheta:g}')
    print(f'NPHI       = {args.nphi:g}')
    print(f'REFINE-R   = {args.refine_r:g}')
    print(f'REFINE-THET= {args.refine_theta:g}')
    print(f'NPHOT-THERM= {args.nphot_therm:g}')
    print(f'NPHOT-SCAT = {args.nphot_scat:g}')

    # Convert units
    args.mstar = args.mstar.to(MASS_UNIT)
    args.rstar = args.rstar.to(LENGTH_UNIT)
    args.tstar = args.tstar.to(TEMP_UNIT)
    args.rmin = args.rmin.to(LENGTH_UNIT)
    args.rmax = args.rmax.to(LENGTH_UNIT)
    args.r0 = args.r0.to(LENGTH_UNIT)
    args.sigma0 = args.sigma0.to(SIGMA_UNIT)
    args.grain_rho = args.grain_rho.to(RHO_UNIT)
    args.grain_minsize = args.grain_minsize.to(LENGTH_UNIT)
    args.grain_maxsize = args.grain_maxsize.to(LENGTH_UNIT)

    return args

# Make the subdirectory to put the radmc setup into
def make_directory(args):
    if not os.path.isdir(DUST_DIR):
        os.mkdir(DUST_DIR)

    if not os.path.isdir(args.name):
        os.mkdir(args.name)

# Write the wavelength_micron.inp file
def write_wavelength(args):
    lam1 = Quantity('1e-1 um').to(WAVELENGTH_UNIT)
    lam2 = Quantity('1e+5 um').to(WAVELENGTH_UNIT)
    nlam = 500
    lam  = np.logspace(np.log10(lam1.value),np.log10(lam2.value),nlam,endpoint=True) * WAVELENGTH_UNIT

    # Write the wavelength file
    filename = 'wavelength_micron.inp'
    filepath = os.path.join(DUST_DIR, filename)
    if not os.path.isfile(filepath):
        with open(filepath, 'w') as f:
            f.write('%d\n' % (len(lam)))
            for v in lam:
                f.write('%13.6e\n' % (v.to(WAVELENGTH_UNIT).value))

    if not os.path.exists(os.path.join(args.name, filename)):
        relpath = os.path.relpath(filepath, args.name)
        os.symlink(relpath, os.path.join(args.name, filename))

    # Write the stars.inp file
    with open(os.path.join(args.name, 'stars.inp'), 'w') as f:
        f.write(f'2\n')
        f.write(f'1 {len(lam)}\n\n')
        f.write('%13.6e %13.6e %13.6e %13.6e %13.6e\n\n' % (
            args.rstar.to(LENGTH_UNIT).value,
            args.mstar.to(MASS_UNIT).value,
            0.0, 0.0, 0.0))
        for v in lam:
            f.write('%13.6e\n' % (v.to(WAVELENGTH_UNIT).value))
        f.write('\n%13.6e\n' % (-args.tstar.to(TEMP_UNIT).value))

# Make sure that the dust files have been created properly
def write_dust(args):
    # Create the dust grain bin sizes (equal mass wrt distribution in each)
    size_power = 4 + args.dust_power_index
    mass_fraction = np.arange(args.ndust+1) / args.ndust
    args.grain_boundaries = (
        mass_fraction * args.grain_maxsize ** size_power
        + (1 - mass_fraction) * args.grain_minsize ** size_power) ** (1/size_power)

    # Dust opacity control file
    with open(os.path.join(args.name, 'dustopac.inp'), 'w') as f:
        f.write(f'2               Format number of this file\n')
        f.write(f'{args.ndust}              Nr of dust species\n')
        f.write(f'============================================================================\n')

        for gsize_min, gsize_max in zip(args.grain_boundaries[:-1], args.grain_boundaries[1:]):
            gsize_min = gsize_min.to('um').value
            gsize_max = gsize_max.to('um').value
            kapscat_name = f'{args.grain_material}-{gsize_min:.3f}um-{gsize_max:.3f}um-{-args.dust_power_index:g}pi-c5'
            opacfilename = f'dustkapscatmat_{kapscat_name}.inp'

            if not os.path.isfile(os.path.join(DUST_DIR, opacfilename)):
                command = [
                    'optool',
                    f'-{args.grain_material}',
                    '-s',
                    '-chop', '5',
                    '-a', f'{gsize_min:g}', f'{gsize_max:g}', f'{-args.dust_power_index}',
                    '-l', 'wavelength_micron.inp',
                    '-radmc', kapscat_name,
                ]
                print("RUNNING:", command)
                subprocess.run(args=command, cwd=DUST_DIR)

            if not os.path.exists(os.path.join(args.name, opacfilename)):
                relpath = os.path.relpath(os.path.join(DUST_DIR, opacfilename), args.name)
                os.symlink(relpath, os.path.join(args.name, opacfilename))

            f.write(f'10              Way in which this dust species is read\n')
            f.write(f'0               0=Thermal grain\n')
            f.write(f'{kapscat_name}        Extension of name of dustkapscatmat_***.inp file\n')
            f.write(f'----------------------------------------------------------------------------\n')

# Make the grid
def make_grid(args):
    ri = np.logspace(
        np.log10(args.rmin.value),
        np.log10(args.rmax.value),
        args.nr+1) * LENGTH_UNIT
    if args.refine_r:
        print('Refining resolution of disc inner edge')
        nlev_rin = 12       # Number of cycles
        nspan_rin = 3       # Number of cells each cycle
        ri = grid_refine_inner_edge(ri,nlev_rin,nspan_rin)
    rc = (ri[:-1] * ri[1:]) ** 0.5

    thetaup = 0.5*np.pi - np.arctan(3.5 * args.aspect_ratio)
    thetai = np.linspace(thetaup, 0.5*np.pi, args.ntheta+1)
    if args.refine_theta:
        print('Refining resolution of mid plane')
        zr = thetai[::-1]
        nlev_zr = 7          # Number of cycles
        nspan_zr = 3         # Number of cells each cycle
        zr = grid_refine_inner_edge(zr,nlev_zr,nspan_zr)
        thetai = zr[::-1]
    thetac = 0.5 * ( thetai[:-1] + thetai[1:] )

    # Store grid back on the args object
    args.ri = ri
    args.rc = rc
    args.nr = len(rc)           # To account for any refinement methods applied
    args.thetai = thetai
    args.thetac = thetac
    args.ntheta = len(thetac)   # To account for any refinement methods applied

    print(f'Grid size ({args.nr}, {args.ntheta}, {args.nphi})')

# Write the grid file
def write_grid(args):
    with open(os.path.join(args.name, 'amr_grid.inp'), 'w') as f:
        f.write(f'1\n')                       # iformat
        f.write(f'0\n')                       # AMR grid style  (0=regular grid, no AMR)
        f.write(f'100\n')                     # Coordinate system: spherical
        f.write(f'0\n')                       # gridinfo
        f.write(f'1 1 0\n')                   # Include r,theta coordinates
        f.write(f'{args.nr} {args.ntheta} {args.nphi}\n')  # Size of grid
        for v in args.ri:
            f.write('%13.6e\n' % (v.to(LENGTH_UNIT).value))      # X coordinates (cell walls)
        for v in args.thetai:
            f.write('%17.10e\n' % (v))     # Y coordinates (cell walls) (use higher precision here)
        f.write('%13.6e\n' % (0))              # Z coordinates (cell walls)
        f.write('%13.6e\n' % (2*np.pi))        # Z coordinates (cell walls)

# Write the density file
def write_density(args):
    # Make the 2-D mesh grid
    rr, tt = np.meshgrid(args.rc, args.thetac, indexing='ij')

    sigma_rr = args.sigma0 * (rr/args.r0)**-1.0
    height_rr = rr * args.aspect_ratio * (rr/args.r0)**0.25
    zz_rt = rr / np.tan(args.thetac)
    vsize_rt = rr / np.tan(args.thetai[np.newaxis,:-1]) - rr / np.tan(args.thetai[np.newaxis,1:])

    rhog = sigma_rr / ((2*np.pi)**0.5 * height_rr) * np.exp(-0.5 * ((zz_rt / height_rr)**2).to(''))
    rhog /= 2*np.sum(rhog*vsize_rt, axis=1, keepdims=True)
    rhog *= sigma_rr
    rhog = rhog.to(RHO_UNIT)

    size_power = 4 + args.dust_power_index
    dust_scale = args.dust_ratio / (args.grain_boundaries[-1]**size_power - args.grain_boundaries[0]**size_power)

    with open(os.path.join(args.name, 'dust_density.binp'), 'wb') as f:
        f.write(struct.pack('q', 1))    # Format number
        f.write(struct.pack('q', 4))    # 4 byte floating point values used
        f.write(struct.pack('q', args.nr * args.ntheta))
        f.write(struct.pack('q', args.ndust))

        for gsize_min, gsize_max in zip(args.grain_boundaries[:-1], args.grain_boundaries[1:]):
            # Compute grain size that split bin into two equal mass halves
            gsize = (0.5 * (gsize_max**size_power + gsize_min**size_power)) ** (1/size_power)
            st_mid_rr = 0.5*np.pi * gsize.to(LENGTH_UNIT) * args.grain_rho / sigma_rr
            mass_fraction = dust_scale * (gsize_max**size_power - gsize_min**size_power)

            rhod = np.exp(-st_mid_rr/args.alpha * (np.exp(0.5 * (zz_rt / height_rr)**2) - 1))
            rhod *= rhog
            rhod /= 2*np.sum(rhod*vsize_rt, axis=1, keepdims=True)
            rhod *= sigma_rr * mass_fraction
            rhod = rhod.to(RHO_UNIT)

            data = rhod.ravel(order='F').astype(np.float32)
            data.value.tofile(f)

            print(f'gsize_min={gsize_min.to("um"):9.3f}, gsize={gsize.to("um"):9.3f}, gsize_max={gsize_max.to("um"):9.3f}, mass_fraction={mass_fraction:7.5f}')

# Write the radmc3d.inp control file
def write_control(args):
    with open(os.path.join(args.name, 'radmc3d.inp'), 'w') as f:
        f.write(f'nphot_therm = {args.nphot_therm}\n')
        f.write(f'nphot_scat = {args.nphot_scat}\n')
        f.write(f'scattering_mode_max = 1\n')
        f.write(f'iranfreqmode = 1\n')
        f.write(f'setthreads = {os.cpu_count()}\n')
        f.write(f'rto_style = 3\n')
        f.write(f'rto_single = 1\n')
        f.write(f'istar_sphere = 1\n')

if __name__ == '__main__':
    args = parse_args()
    make_directory(args)
    write_wavelength(args)
    write_dust(args)
    make_grid(args)
    write_grid(args)
    write_density(args)
    write_control(args)
